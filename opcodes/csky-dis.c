
#include "cskyv1-dis.h"
#include "cskyv2-dis.h"
#include "cskyv2p-dis.h"
#include "dis-asm.h"
#include "elf-bfd.h"

// only for objdump tool
#define INIT_MACH_FLAG  0xffffffff
static unsigned int mach_flag = INIT_MACH_FLAG;
disassembler_ftype csky_get_disassembler (bfd *abfd)
{
    mach_flag = elf_elfheader (abfd)->e_flags;
    return print_insn_csky;
}

int
print_insn_csky(bfd_vma memaddr, struct disassemble_info *info)
{
  if(INIT_MACH_FLAG != mach_flag) info->mach = mach_flag;
  if((DISAS_INSN_VERSION((info->mach)) == INSN_V1 ) || (info->mach == 0))
  {
    return v1_print_insn_csky(memaddr, info);
  }
  else if (DISAS_INSN_VERSION(info->mach) == INSN_V2)
  {
    return v2_print_insn_csky(memaddr, info);
  }
  else 
  {
    return v2p_print_insn_csky(memaddr, info);
  }
}

