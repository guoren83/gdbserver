/* Motorola CSKY support for BFD.
   Copyright 1995, 1999, 2000 Free Software Foundation, Inc.

This file is part of BFD, the Binary File Descriptor library.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301, USA.  */

/* This file holds definitions specific to the CSKY ELF ABI. */
#ifndef _ELF_CSKY_H
#define _ELF_CSKY_H

#include "elf/reloc-macros.h"

/* Values of relocation types according to the ABI doc*/
START_RELOC_NUMBERS (elf_csky_reloc_type)
    RELOC_NUMBER(R_CKCORE_NONE,0)
    RELOC_NUMBER(R_CKCORE_ADDR32,1)
    RELOC_NUMBER(R_CKCORE_PCREL_IMM8BY4,2)
    RELOC_NUMBER(R_CKCORE_PCREL_IMM11BY2,3)
    RELOC_NUMBER(R_CKCORE_PCREL_IMM4BY2,4)
    RELOC_NUMBER(R_CKCORE_PCREL32,5)
    RELOC_NUMBER(R_CKCORE_PCREL_JSR_IMM11BY2,6)
    RELOC_NUMBER(R_CKCORE_GNU_VTINHERIT,7)
    RELOC_NUMBER(R_CKCORE_GNU_VTENTRY,8)
    RELOC_NUMBER(R_CKCORE_RELATIVE,9)
    RELOC_NUMBER(R_CKCORE_COPY,10)
    RELOC_NUMBER(R_CKCORE_GLOB_DAT,11)
    RELOC_NUMBER(R_CKCORE_JUMP_SLOT,12)
    RELOC_NUMBER(R_CKCORE_GOTOFF,13)
    RELOC_NUMBER(R_CKCORE_GOTPC,14)
    RELOC_NUMBER(R_CKCORE_GOT32,15)
    RELOC_NUMBER(R_CKCORE_PLT32,16)
    RELOC_NUMBER(R_CKCORE_ADDRGOT,17)
    RELOC_NUMBER(R_CKCORE_ADDRPLT,18)
    RELOC_NUMBER(R_CKCORE_PCREL_IMM26BY2,19)
    RELOC_NUMBER(R_CKCORE_PCREL_IMM16BY2,20)
    RELOC_NUMBER(R_CKCORE_PCREL_IMM16BY4,21)
    RELOC_NUMBER(R_CKCORE_PCREL_IMM10BY2,22)
    RELOC_NUMBER(R_CKCORE_PCREL_IMM10BY4,23)
    RELOC_NUMBER(R_CKCORE_ADDR_HI16,24)
    RELOC_NUMBER(R_CKCORE_ADDR_LO16,25)
    RELOC_NUMBER(R_CKCORE_GOTPC_HI16,26)
    RELOC_NUMBER(R_CKCORE_GOTPC_LO16,27)
    RELOC_NUMBER(R_CKCORE_GOTOFF_HI16,28)
    RELOC_NUMBER(R_CKCORE_GOTOFF_LO16,29)
    RELOC_NUMBER(R_CKCORE_GOT12,30)
    RELOC_NUMBER(R_CKCORE_GOT_HI16,31)
    RELOC_NUMBER(R_CKCORE_GOT_LO16,32)
    RELOC_NUMBER(R_CKCORE_PLT12,33)
    RELOC_NUMBER(R_CKCORE_PLT_HI16,34)
    RELOC_NUMBER(R_CKCORE_PLT_LO16,35)
    RELOC_NUMBER(R_CKCORE_ADDRGOT_HI16,36)
    RELOC_NUMBER(R_CKCORE_ADDRGOT_LO16,37)
    RELOC_NUMBER(R_CKCORE_ADDRPLT_HI16,38)
    RELOC_NUMBER(R_CKCORE_ADDRPLT_LO16,39)
    RELOC_NUMBER(R_CKCORE_PCREL_JSR_IMM26BY2,40)
    RELOC_NUMBER(R_CKCORE_TOFFSET_LO16, 41)
    RELOC_NUMBER(R_CKCORE_DOFFSET_LO16, 42)
    RELOC_NUMBER(R_CKCORE_PCREL_IMM18BY2, 43)
    RELOC_NUMBER(R_CKCORE_DOFFSET_IMM18, 44)
    RELOC_NUMBER(R_CKCORE_DOFFSET_IMM18BY2, 45)
    RELOC_NUMBER(R_CKCORE_DOFFSET_IMM18BY4, 46)
    RELOC_NUMBER(R_CKCORE_GOTOFF_IMM18, 47)
    RELOC_NUMBER(R_CKCORE_GOT_IMM18BY4, 48)
    RELOC_NUMBER(R_CKCORE_PLT_IMM18BY4, 49)
END_RELOC_NUMBERS (R_CKCORE_MAX)

#define INSN_MASK 0XFFFF
#define INSN_V1    1
#define INSN_V2    2
#define INSN_V2P   3

#define DISAS_INSN_VERSION(flag) \
      (((flag&ARCH_MASK) == bfd_mach_ck510  \
	   || (flag&ARCH_MASK) == bfd_mach_ck520 \
	   || (flag&ARCH_MASK) == bfd_mach_ck610 \
	   || (flag&ARCH_MASK) == bfd_mach_ck620 ) ? INSN_V1: \
           ((( flag&ARCH_MASK) == bfd_mach_ck802p \
               ||( flag&ARCH_MASK) ==  bfd_mach_ck803p \
	       ||( flag&ARCH_MASK) == bfd_mach_ck810p \
	       ||( flag&ARCH_MASK) == bfd_mach_ck803a )?  INSN_V2P : INSN_V2))

#define INSN_VERSION(flag)  ((flag & (M_CK510 | M_CK610)) ? INSN_V1 : (flag & (INSN_CK810P | INSN_CK803P | INSN_CK802P ) ?  INSN_V2P: INSN_V2))

#endif /* _ELF_CSKY_H */

