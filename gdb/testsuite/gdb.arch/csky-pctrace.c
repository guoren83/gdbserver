
int func1(int a)
{
   return a+1;
}

int func2(int a)
{
   return a+2;
}

int func3(int a)
{ 
   return a+3;
}

int func4(int a)
{  
   return a+4;
}

int main()
{
   int a=0;
   a=func1(a);
   if (a>0)
   {
       a+=1;
   }
   a=func2(a);
   if(a==4)
   {
       a+=1;
   }
   a=func3(a);
   return a;  
}
