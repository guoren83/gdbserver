# You'll find a bunch of nop opcodes in the below macros.  They are
# there to keep the code correctly aligned.  Be careful to maintain
# them when changing the code.
	
	comment "subroutine declare"
	.purgem gdbasm_declare
	.macro gdbasm_declare name
	.align  1
	.global \name
\name:	
	.endm

	 comment "subroutine prologue"
	.macro gdbasm_enter
	 subi    r0, 28 
	 subi    r0, 32
	 stw     r1, (r0, 0)
	stw     r2, (r0, 4)
	stw     r3, (r0, 8)
	stw     r4, (r0, 12)
	stw     r5, (r0, 16)
	stw     r6, (r0, 20)
	stw     r7, (r0, 24)
	stw     r8, (r0, 28)
	stw     r9, (r0, 32)
	stw     r10, (r0, 36)
	stw     r11, (r0, 40)
	stw     r12, (r0, 44)
	stw     r13, (r0, 48)
	stw     r14, (r0, 52)
	stw     r15, (r0, 56)
	.endm
	 comment "subroutine epilogue"
	.macro gdbasm_leave
	ldw     r2, (r0, 4)
	ldw     r3, (r0, 8)
	ldw     r4, (r0, 12)
	ldw     r5, (r0, 16)
	ldw     r6, (r0, 20)
	ldw     r7, (r0, 24)
	ldw     r8, (r0, 28)
	ldw     r9, (r0, 32)
	ldw     r10, (r0, 36)
	ldw     r11, (r0, 40)
	ldw     r12, (r0, 44)
	ldw     r13, (r0, 48)
	ldw     r14, (r0, 52)
	ldw     r15, (r0, 56)
	
	addi    r0,32
	addi    r0,28
        rts	
	.endm

        comment "subroutine end"
        .purgem gdbasm_end
        .macro gdbasm_end name
        .size   \name, .-_foo1
        .align  1
        .endm

        comment "subroutine call"
	.macro gdbasm_call subr
	jbsr    \subr
	.endm
	

	.macro gdbasm_several_nops
	nop
	nop
	nop
	nop
	.endm
	
	comment "exit (0)"
	.macro gdbasm_exit0
         movi r2,0
         bkpt 	 
	.endm




comment "Declare a data variable"
.purgem gdbasm_datavar
.macro gdbasm_datavar name value
.data
\name:
.long   \value
.endm

comment "crt0 startup"
.macro gdbasm_startup
    nop
.endm

