/*
 * csky-rtos.h
 *
 *  Created on: 2015-5-1
 *      Author: Perer_JL
 */

#ifndef CSKY_RTOS_H_
#define CSKY_RTOS_H_

#define TASK_NAME_MAX	64
#define RTOS_BASIC_FIELD_NUM 5
#define CSKY_RTOS_NUM   6

extern void init_rtos_ops();
extern struct target_ops* get_csky_ops();
extern void prepare_csky_ops(struct target_ops* ops);

typedef enum rtos_data_type
{
  // int
  Integer,
  // a char*
  String,
  // display as const char*, but saved as int in the target
  IntToString,
  // core_addr
  CoreAddr,
  // other data type
  Undefine
} RTOS_DATA_TYPE;

typedef union _rtos_field
{
  int IntVal;
  char * string;
  void * undef;
  CORE_ADDR coreaddr;
} RTOS_FIELD;

/**
 * RTOS extends field descriptor
 * This struct will be used to calculate the field
 */
typedef struct rtos_field_des
{
  // human readable name of this field
  const char * name;
  // the data type
  RTOS_DATA_TYPE type;
  // the size of this data
  unsigned char size;
  // a transfer method if type is IntToString type
  char * (*int2StringTransfer)(int index);
  // flag to indicate whether use default handler or not
  // 1: use default handler, will ignore to_read_field&to_write_field
  // 0: use specified read/write handler: to_read_field&to_write_field
  int use_default;
  // if a field is not directly in target TCB, more than once transfer should be needed;
  // num of elememts of offset_table
  int offset_level;
  const char ** offset_table;
  // should the same size as offset_table, and init value is -1
  int *offset_cache;
  // specified handler extends by user
  int (*to_read_field)(CORE_ADDR , struct rtos_field_des* , RTOS_FIELD *);
  // output value of RTOS_FIELD
  void (*to_output_field)(struct rtos_field_des* itself, RTOS_FIELD *val, int from_tty);
} RTOS_FIELD_DES;

/**
 * required fields when csky multi-tasks debugging
 */
typedef struct rtos_tcb
{
  // to indicate how many global task list in the RTOS's kernel
  unsigned int task_list_count;
  // thread_list, current_thread, list_next
  // for some RTOS(nuttx), the there is more than one global task list in kernel;
  // we prepare 10 to garantte to get all the task list in RTOS
  // if there is less then 10 in the RTOS, just leave it alone, our GDB will do nothing
  // see $rtos_task_list_count for more information
  RTOS_FIELD_DES rtos_special_table[10][3];
  // task_id, stack_ptr, entry_base, tcb_base, task_name
  RTOS_FIELD_DES rtos_tcb_table[5];

  // the number of elements in rtos_tcb_extend_table
  unsigned int extend_table_num;
  // table to describe the way to get all extend field
  RTOS_FIELD_DES* rtos_tcb_extend_table;

  // the following is used to implements all "info mthreads" commands
  // the table contains index for rtos_tcb_extend_table which is used to get the value
  unsigned int *i_mthreads_list;
  unsigned int i_mthreads_list_size;
  unsigned int *i_mthreads_stack;
  unsigned int i_mthreads_stack_size;
  unsigned int *i_mthread_one;
  unsigned int i_mthread_one_size;

  // rtos register offset table
  int *rtos_reg_offset_table;

  // for some RTOS, the registers are not save in the stack, and they are in a globle block
  // this handler will return the start address of this blobal block
  // tcb_base: the tcb_base address of a task
  // return the register base address for the given task
  CORE_ADDR (*to_get_register_base_address) (CORE_ADDR tcb_base, int regno);

  // the read/write register handler
  // tcb_base: cur task tcb addr
  // stack_ptr: current stack pointer of this task
  // regno: the number of register
  // val: the value to be read/written
  // return 0: the reg is in the memory pointed by stack_ptr, else reg is in CPU
  void (*to_fetch_registers) (CORE_ADDR tcb_base, CORE_ADDR stack_ptr,
                              int regno, unsigned int* val);
  void (*to_store_registers) (CORE_ADDR tcb_base, CORE_ADDR stack_ptr,
                              int regno, unsigned int val);
  // to check if this thread_id is valid
  int (* IS_VALID_TASK) (RTOS_FIELD thread_id);
  // To check if this reg in task's stack
  int (*is_regnum_in_task_list)(struct rtos_tcb*, ptid_t, int regno);
} RTOS_TCB;

typedef struct rtos_event
{
  /* events : flag, mutex, message, sema
   * special fields : event_base, event_next
   */
  RTOS_FIELD_DES rtos_event_special_table[4][2];

  // fields for each event: id, type, name, pending list
  RTOS_FIELD_DES rtos_event_info_table[4][4];
}RTOS_EVENT;



enum RTOS_STATUS
{
  // no rtos program in target
  NO_RTOS_PROGRAME,
  // rtos is not loaded into target
  NOT_INIT_TARGET,
  // valid target
  VALID_TARGET
};

/**
 * rtos_ops used to extends csky_ops(struct target_ops)
 * to support csky multi-tasks debugging
 */
typedef struct
{
  // the current target ops
  struct target_ops* current_ops;
  // rtos tcb descriptor
  RTOS_TCB * rtos_des;
  RTOS_EVENT *event_des;
  /* Keep consistent with current thread in the target-board. */
  ptid_t target_ptid;

  // update event info from target
  void (*to_update_event_info)(struct target_ops*, RTOS_EVENT* rtos_event_des);
  /**
   * 1. security check for the specified target;
   * 2. check whether target has the corresponding rtos program
   * 3. check whether target is running the rtos program
   * 4. update the tasks info from target
   * return RTOS_STATUS to indicate 1. 2. 3.
   */
  enum RTOS_STATUS (*to_update_task_info)(struct target_ops*, RTOS_TCB*, ptid_t*);
  /**
   * hook when target ${RTOS} IP:PORT is executed
   * initialize current kernel ops no matter whatever.
   */
  void (*to_open)(struct target_ops*, RTOS_TCB*);
  /**
   * initialize current kernel ops no matter whatever.
   */
  void (*to_close)(struct target_ops*, RTOS_TCB*);
  /**
   * check if the register is in RTOS_TCB
   * return : 0: in RTOS_TCB; other: not there
   */
  int (*is_regnum_in_task_list)(RTOS_TCB*, ptid_t, int regno);
  /**
   * to get the register value
   * return 0: regno is in task list, else not in the task list
   */
  void (*to_fetch_registers)(RTOS_TCB*, ptid_t, int regno, unsigned int* value);
  /**
   * to store the register value
   * return 0: regno is in task list, else not in the task list
   */
  void (*to_store_registers)(RTOS_TCB*, ptid_t, int regno, ULONGEST value);
  /**
   * preparation for resume, does user switch the thread ?
   */
  void (*to_prepare_resume)(RTOS_TCB*, ptid_t, int step);
  /**
   * check whether the given ptid in the RTOS_TCB list
   * return 1: in the list; 0: not in the list
   */
  int (*is_task_in_task_list) (RTOS_TCB*, ptid_t);
  /*
   * Change the PID into the char pointer for GDB to print out.
   */
  void (*to_pid_to_str)(RTOS_TCB*, ptid_t ptid, char *buf);
  /**
   * reset the rtos_tcb list
   */
  void (*to_reset)(RTOS_TCB*);
} RTOS_OPS;

extern RTOS_OPS rtos_ops;

typedef struct
{
  struct target_ops * ops;
  void (*to_open) (char *, int);
  // all names which should use corresponding target_ops
  const char ** names;
  // the number of elemenst in string array names;
  unsigned int name_num;
  // used to decribe the TCB
  RTOS_TCB* rtos_tcb_des;

  // used to decribe the events
  RTOS_EVENT* rtos_event_des;

  // init rtos_tcb
  void (*init_rtos_tcb)(void);

  // init rtos_event
  void (*init_rtos_event)(void);
} RTOS_INIT_TABLE;

/**
 * required fields when csky multi-tasks debugging
 * This struct is used to cache all task info in the target
 */
typedef struct rtos_tcb_common
{
  struct rtos_tcb_common * next;
  // task_id, stack_ptr, entry_base, tcb_base, task_name
  RTOS_FIELD rtos_basic_table[5];
  // the extends data table, the data type is
  RTOS_FIELD * extend_fields;
  /* Flag for refresh_rtos_task_list()
     if accessed = 1: this member is accessed
     else, member is not accessed. */
  unsigned int accessed;
} RTOS_TCB_COMMON;

/**
 * This struct is used to cache all task event info in the target
 */
typedef struct rtos_event_common
{
  struct rtos_event_common * next;
  // id, type, name, pendlist
  RTOS_FIELD rtos_event_info_table[4];
  /* Flag for refresh_rtos_task_list()
     if accessed = 1: this member is accessed
     else, member is not accessed. */
  unsigned int accessed;
} RTOS_EVENT_COMMON;

extern  RTOS_TCB_COMMON* find_rtos_task_info(ptid_t);
extern int csky_rtos_symbol2offset(char *s, int *offset);
extern void common_open(char * name, int from_tty);
extern void initialize_rtos_init_tables();
extern RTOS_INIT_TABLE rtos_init_tables[CSKY_RTOS_NUM];
extern int csky_rtos_parse_field_default(CORE_ADDR tcb_base,RTOS_FIELD_DES *f_des, RTOS_FIELD * field);
extern void  csky_rtos_output_field_default(RTOS_FIELD_DES* itself, RTOS_FIELD *val, int from_tty);

extern void set_is_rtos_ops();
extern int is_rtos_ops();
#endif /* CSKY_RTOS_H_ */

