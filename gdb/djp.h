// ****************************************************************************
// *                                                                          *
// * C-Sky Microsystems Confidential                                          *
// * -------------------------------                                          *
// * This file and all its contents are properties of C-Sky Microsystems. The *
// * information contained herein is confidential and proprietary and is not  *
// * to be disclosed outside of C-Sky Microsystems except under a             *
// * Non-Disclosure Agreement (NDA).                                          *
// *                                                                          *
// ****************************************************************************



///////////////////////////////////////////////////////////////////////////////
// File name: djp.h
// function description: proxy structure and error definition.
//                       It is the same in GDB JTAG definition.
//

#ifndef __CKCORE_DEBUGGER_SERVER_DJP_H__
#define __CKCORE_DEBUGGER_SERVER_DJP_H__

#ifndef __CKDATATYPE__
#define __CKDATATYPE__
typedef unsigned int   U32;
typedef int            S32;
typedef unsigned short U16;
typedef short          S16;
typedef unsigned char  U8;
typedef char           S8;
#endif

//For arch info
#define CSKY_BIG_ENDIAN      1
#define CSKY_LITTLE_ENDIAN   0

#define CSKY_SOCKET_PACKAGE_SIZE 4096

/* Possible djp errors are listed here.  */
enum enum_errors
{
  /* Codes > 0 are for system errors */

  ERR_NONE = 0,
  ERR_CRC = -1,
  ERR_MEM = -2,
  JTAG_PROXY_INVALID_COMMAND = -3,
  JTAG_PROXY_SERVER_TERMINATED = -4,
  JTAG_PROXY_NO_CONNECTION = -5,
  JTAG_PROXY_PROTOCOL_ERROR = -6,
  JTAG_PROXY_COMMAND_NOT_IMPLEMENTED = -7,
  JTAG_PROXY_INVALID_CHAIN = -8,
  JTAG_PROXY_INVALID_ADDRESS = -9,
  JTAG_PROXY_ACCESS_EXCEPTION = -10, /* Write to ROM */
  JTAG_PROXY_INVALID_LENGTH = -11,
  JTAG_PROXY_OUT_OF_MEMORY = -12,
  JTAG_NON_DEBUG_REGION = -13,   // cpu in non-debug region, cpu can't enter debug
};

/* All JTAG chains.  */
enum jtag_chains
  {
    SC_GLOBAL,      /* 0 Global BS Chain */
    SC_RISC_DEBUG,  /* 1 RISC Debug Interface chain */
    SC_RISC_TEST,   /* 2 RISC Test Chain */
    SC_TRACE,       /* 3 Trace Chain */
    SC_REGISTER,    /* 4 Register Chain */
    SC_WISHBONE,    /* 5 Wisbone Chain */
    SC_BLOCK        /* Block Chains */
  };


///////////////////////////////////////////////////////////////////////////////
// Command definition of Proxy Function
//
typedef enum
{
	DBGCMD_REG_READ			= 1,
	DBGCMD_REG_WRITE		= 2,
	DBGCMD_BLOCK_READ_OLD		= 3,
	DBGCMD_BLOCK_WRITE_OLD		= 4,
	DBGCMD_ENTER_DEBUG		= 5,
	DBGCMD_CHECK_DEBUG		= 6,
	DBGCMD_SINGLESTEP		= 7,
	DBGCMD_RUN			= 8,
	DBGCMD_SYSTEM_RESET		= 9,
	DBGCMD_BLOCK_LOAD_OLD		= 10,
	DBGCMD_INSERT_WATCHPOINT_OLD	= 11,
	DBGCMD_REMOVE_WATCHPOINT_OLD	= 12,
	DBGCMD_MEM_READ			= 13,
	DBGCMD_MEM_WRITE		= 14,
	DBGCMD_INSERT_HW_BREAKPOINT	= 16,
	DBGCMD_REMOVE_HW_BREAKPOINT	= 17,
	DBGCMD_SEND_LOADINFO_OLD	= 18,
	DBGCMD_TRACE_PC			= 19,
	DBGCMD_ENDIAN_INFO		= 20,
	DBGCMD_INSERT_WATCHPOINT	= 21,
	DBGCMD_REMOVE_WATCHPOINT	= 22,
	DBGCMD_SET_PROFILING_INFO	= 23,
	DBGCMD_GET_PROFILING_INFO	= 24,
	DBGCMD_VERSION_INFO		= 25,
	DBGCMD_XREG_READ		= 26,
	DBGCMD_XREG_WRITE		= 27,
	DBGCMD_HAD_REG_READ		= 28,
        DBGCMD_HAD_REG_WRITE            = 29,
        DBGCMD_SYSTEM_SOFT_RESET        = 30,
        DBGCMD_HW_BKPT_NUM              = 31,
        DBGCMD_XML_TDESC_READ           = 32,
        DBGCMD_DEL_ALL_HWBKPT           = 33
} DBG_CMD;



///////////////////////////////////////////////////////////////////////////////
// Error definition of Proxy Function
//
typedef enum
{
	DJP_ERR_NONE = 0,                              // success
	DJP_PROTOCOL_ERROR = -6,	           // command actual length is error
	DJP_COMMAND_NOT_IMPLEMENTED = -7,   // command is not implemented
	DJP_OUT_OF_MEMORY = -12,	           // fail in mallocing memory
	DJP_OPERATION_TIME_OUT = -13,    // time out when entering debug mode
	DJP_COMMUNICATION_TIME_OUT = -14,   // communication(ICE) time out
	DJP_TARGET_NOT_CONNECTED = -15,     // no connect with target board
	DJP_REG_NOT_DEFINED = -16,          // register ID is not defined
	DJP_ADDRESS_MISALIGNED = -17,   // address is not matched with mode
	DJP_DATA_LENGTH_INCONSISTENT = -18, // data length error
	DJP_INVALID_BKPT = -19,             // third BKPT is not allowed
	DJP_DEL_NONEXISTENT_BKPT = -20, // no BKPT in specified address
	DJP_MASK_MISALIGNED = -21,      // address isn't cosistent with mask
	DJP_NO_PROFILING = -22,         // no profilling function in target
	DJP_CPU_ACCESS = -23,           // Reserved (CPU access right)
	DJP_ADDRESS_ACCESS = -24,       // Reserved (address access right)
	DJP_COMMAND_FORMAT = -25,       // Command format error
	DJP_COMMAND_EXECUTE = -26,      // Command is not finished
        DJP_NON_DEBUG_REGION = -27,     // CPU in non-debug region
        DJP_NO_MORE_HWBKPT_HWWP = -28,  // No more hwbkpt or hw-watchpoint
} PROXY_ERROR;


///////////////////////////////////////////////////////////////////////////////
// Data Structure Of Proxy Communication
// Including : 1. receive massage structure
//             2. send response structure
//

// ------------------------------------
// DBGCMD_REG_READ
typedef struct{
	U32 command; 	// read reg: 1
	U32 length;	// length of frame
	U32 address;	// no. of reg
} ReadRegMsg;

typedef struct {
	S32 status;
	U32 data_H;
	U32 data_L;
} ReadRegRsp;
//-------------------for read had reg---------------------
typedef struct{
        U32 command;    // read reg: 1
        U32 length;     // length of frame
        U32 address;    // no. of reg
} ReadHadRegMsg;

typedef struct {
        S32 status;
        U32 data;
} ReadHadRegRsp;

typedef struct{       //get bkpt num
	U32 command;
	U32 length;
} HwBkptNumMsg;

typedef struct{
	S32 status;
	U32 data1;
	U32 data2;
} HwBkptNumRsp;

//----------------------------------------------------------
// DBGCMD_HAD_REG_WRITE
typedef struct {
        U32 command;    // write reg: 2
        U32 length;     // length of frame
        U32 address;    // no. of reg
        U32 data;
} WriteHadRegMsg;

typedef struct {
        S32 status;
} WriteHadRegRsp;
//-------------------------------------------------------
// ------------------------------------
// DBGCMD_REG_WRITE
typedef struct {
	U32 command; 	// write reg: 2
	U32 length;	// length of frame
	U32 address;	// no. of reg
	U32 data_H;
	U32 data_L;
} WriteRegMsg;

typedef struct {
	S32 status;
} WriteRegRsp;


// ------------------------------------
// DBGCMD_MEM_READ
typedef struct {
	U32 command;	// read memory: 13
	U32 length;	// length of frame
	U32 address;	// write address
	U32 nLength;	// length of data
	U32 nSize;	// data Unit
} ReadMemMsg;

typedef struct {
	S32 status;
	U32 nLength;	// length of data
	U32 nSize;	// data Unit
	U8 data[0];	// write data pointer
} ReadMemRsp;

// ------------------------------------
// DBGCMD_MEM_WRITE
typedef struct {
	U32 command; 	// write memory: 14
	U32 length;	// length of frame
	U32 address;	// write address
	U32 nLength;	// length of data
	U32 nSize;	//  data Unit
	U8  data[0];	// write data pointer
} WriteMemMsg;

typedef struct {
	S32 status;
} WriteMemRsp;

// ------------------------------------
// DBGCMD_ENTER_DEBUG
typedef struct {
	U32 command; 	// enter debug mode: 5
	U32 length;	// length of frame
	U32 chain;	// enter mode
} EnterDebugMsg;

typedef struct {
	S32 status;
} EnterDebugRsp;

// ------------------------------------
// DBGCMD_CHECK_DEBUG
typedef struct {
	U32 command; 	//check debug mode 6
	U32 length;	// length of frame
} CheckDebugMsg;

typedef struct {
	S32 status;
	U32 debugmode;
} CheckDebugRsp;

// ------------------------------------
// DBGCMD_SINGLESTEP
typedef struct {
	U32 command; 	// single step: 7
	U32 length;	// length of frame
} SingleStepMsg;

typedef struct {
	S32 status;
} SingleStepRsp;

// ------------------------------------
// DBGCMD_RUN
typedef struct {
	U32 command; 	// Run: 8
	U32 length;	// length of frame
} CmdRunMsg;

typedef struct {
	S32 status;
} CmdRunRsp;


// ------------------------------------
// DBGCMD_INSERT_HW_BREAKPOINT
// DBGCMD_REMOVE_HW_BREAKPOINT
typedef struct {
	U32 command; 	// insert: 16   remove: 17
	U32 length;	// length of frame
	U32 address;
} HwBkptMsg;

typedef struct {
	S32 status;
} HwBkptRsp;

#define HW_WRITE   0x5    // enable all data write memory insn
#define HW_READ    0x6    // enable all data read memory insn
#define HW_ACCESS  0x1    // enable all data r/w memory insn
#define HW_EXECUTE 0x2    // enable all text fetch insn
#define HW_NONE    0      // disable all hareware breakpoint/watchpoint

// ------------------------------------
// DBGCMD_INSERT_WATCHPOINT
// DBGCMD_REMOVE_WATCHPOINT
typedef struct {
	U32 command; 	// set: 21   remove:22
	U32 length;	// length of frame
	U32 address;	// start address of watchpoint
	U32 mode;	// tragger mode
	U32 mask;	// address mask
	U32 counter;	// tragger after <counter>
} WatchpointMsg;

typedef struct {
	S32 status;
} WatchpointRsp;

// ------------------------------------
// DBGCMD_SYSTEM_RESET
typedef struct {
	U32 command; 	// system reset 9
	U32 length;	// length of frame
} SystemResetMsg;

typedef struct {
	S32 status;
} SystemResetRsp;

// ------------------------------------
// DBGCMD_SYSTEM_SOFT_RESET
typedef struct {
        U32 command;    // system reset 30
        U32 length;     // length of frame
        U32 insn;
} SystemSoftResetMsg;

typedef struct {
        S32 status;
} SystemSoftResetRsp;

// Fix me: it must be 8, even if the real pcfifo depth is not 8
#define PCFIFO_DEPTH  8

// ------------------------------------
// DBGCMD_TRACE_PC
typedef struct {
	U32 command;	// jump pc trace : 19
	U32 length;	// length of frame
} PcJumpMsg;

typedef struct {
	S32 status;
	U32 pc[PCFIFO_DEPTH];
} PcJumpRsp;

// ------------------------------------
// DBGCMD_SET_PROFILING_INFO
typedef struct {
	U32 command; 	//set profiling info: 23
	U32 length;	//length of frame
	U8 data[0];	//set information
} SetProfilingMsg;

typedef struct {
	S32 status;
} SetProfilingRsp;

// ------------------------------------
// DBGCMD_GET_PROFILING_INFO
typedef struct {
	U32 command; 	// get profiling info : 24
	U32 length;	// length of frame
} GetProfilingMsg;

typedef struct {
	S32 status;
	U32 length;	// length of data
	U8 data[0];	// data
} GetProfilingRsp;

// ------------------------------------
// DBGCMD_VERSION_INFO
typedef struct {
	U32 command; 	//version info: 25
	U32 length;	// length of frame
} VersionMsg;

typedef struct {
	S32 status;
	U32 srv_version;
	U32 had_version[4];
} VersionRsp;

// ------------------------------------
// DBGCMD_ENDIAN_INFO
typedef struct {
	U32 command; 	//version info: 20
	U32 length;	// length of frame
} EndianMsg;

typedef struct {
	S32 status;
	U32 endian;
} EndianRsp;


///////////////////////////////////////////////////////////////////////////////
// For compatibility (reserve the command in old version)


// ------------------------------------
// DBGCMD_BLOCK_WRITE_OLD
typedef struct {
	U32 command; 	// write memory: 4(old version)
	U32 length;	// length of frame
	U32 address;	// write address
	U32 nRegisters; // Write Byte
	U8 data[0];	// write data pointer
} WriteMemMsg_old;

typedef struct {
	S32 status;
} WriteMemRsp_old;

// ------------------------------------
// DBGCMD_BLOCK_READ_OLD
typedef struct {
	U32 command; 	// read memory: 3 (old version)
	U32 length;	// length of frame
	U32 address;	// write address
	U32 nRegisters; // Read Byte
} ReadMemMsg_old;

typedef struct {
	S32 status;
	U32 nRegisters; // Read Byte
	U8 data[0];	// write data pointer
} ReadMemRsp_old;


// ------------------------------------
// DBGCMD_BLOCK_LOAD_OLD
typedef struct {
	U32 command; 	// block load: 10 (old version)
	U32 length;	// length of frame
	U32 address;	// write address
	U32 nRegisters; // Write Byte
	U8 data[0];	// write data pointer
} BlockLoadMsg_old;

typedef struct {
	S32 status;
} BlockLoadRsp_old;

// ------------------------------------
// DBGCMD_INSERT_WATCHPOINT_OLD
// DBGCMD_REMOVE_WATCHPOINT_OLD
typedef struct {
	U32 command; 	// set : 11 Remove: 12 (old version)
	U32 length;	// length of frame
	U32 address;
	U32 mode;	// tragger mode
} WatchpointMsg_old;

typedef struct {
	S32 status;
} WatchpointRsp_old;


// ------------------------------------
// DBGCMD_SEND_LOADINFO_OLD
typedef struct {
	U32 command; 	// Command: 18 (old version)
	U32 length;	// length of frame
	U32 address;	// start address
} StartAddrMsg_old;

typedef struct {
	S32 status;
} StartAddrRsp_old;

// ------------------------------------
//  DBGCMD_XREG_READ
typedef struct {
	U32 command;	// Command: 26
	U32 length;	// length of frame
	U32 address;	// new regnum
	U32 size;       // length of reg value by byte
} ReadXRegMsg;

typedef struct {
	U32 status;
	U8 data[0];
} ReadXRegRsp;
// ------------------------------------
//  DBGCMD_XREG_WRITE
typedef struct {
	U32 command;	// Command: 27
	U32 length;	// length of frame
	U32 address;	// new regnum
	U32 size;	// length of reg value by byte
	U8 data[0];
} WriteXRegMsg;

typedef struct {
	U32 status;
} WriteXRegRsp;

//------------------------------------
//  DBGCMD_XML_TDESC_READ
typedef struct {
        U32 command;    // read xml-tdesc: 32
        U32 length;     // length of frame
        U32 address;    // start address
        U32 nLength;    // length of data buffer
} ReadXmlTdescMsg;

typedef struct {
        S32 status;
        U32 rlen;      // length of data actually get
        U8 data[0];
} ReadXmlTdescRsp;

#endif // __CKCORE_DEBUGGER_SERVER_DJP_H__
