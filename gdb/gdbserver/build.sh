#!/bin/bash
CSKY_TOOL_PATH=~/disk2/source/buildroot/csky_anole_ck810_gerrit_defconfig/host/bin/

mkdir build
cd build
PATH=$PATH:$CSKY_TOOL_PATH ../configure CFLAGS="-mcpu=ck810f" --host=csky-linux --prefix=/tmp/gdbserver
PATH=$PATH:$CSKY_TOOL_PATH make
PATH=$PATH:$CSKY_TOOL_PATH make install
cd -
