#include "sysdep.h"
#include "bfd.h"
#include "bfdlink.h"
#include "libbfd.h"
#include "elf-bfd.h"
#include "elf/csky.h"
#include <assert.h>
#define ASSERTLINE //BFD_ASSERT(1)

/* The index of a howto-item is implicitly equal to 
   the coresponding Relocation Type Encoding. */
static reloc_howto_type csky_elf_howto_table[] = {
  /* 0 */
  HOWTO (R_CKCORE_NONE,		/* type */
	 0,			/* rightshift */
	 0,			/* size */
	 0,			/* bitsize */
	 FALSE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_dont,  /* complain_on_overflow */
	 NULL,                  /* special_function */
	 "R_CKCORE_NONE",	/* name */
	 FALSE,			/* partial_inplace */
	 0,			/* src_mask */
	 0,			/* dst_mask */
	 FALSE),		/* pcrel_offset */
	
  /* 1 */
  HOWTO (R_CKCORE_ADDR32,		/* type */
	 0,			/* rightshift */
	 2,			/* size */
	 32,			/* bitsize */
	 FALSE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_dont,  /* complain_on_overflow */
	 bfd_elf_generic_reloc,       /* special_function */
	 "R_CKCORE_ADDR32",	/* name */
	 FALSE,			/* partial_inplace */
	 0xffffffff,			/* src_mask */
	 0xffffffff,			/* dst_mask */
	 FALSE),		/* pcrel_offset */

  /* 2 : only for V1 */
  HOWTO (R_CKCORE_PCREL_IMM8BY4,	/* type */
	 2,			/* rightshift */
	 1,			/* size */
	 8,			/* bitsize */
	 TRUE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_bitfield, /* complain_on_overflow */
	 NULL,	/* special_function */
	 "R_CKCORE_PCREL_IMM8BY4",		/* name */
	 FALSE,			/* partial_inplace */
	 0xff,			/* src_mask */
	 0xff,		/* dst_mask */
	 TRUE),		/* pcrel_offset */

  /* 3 : only for V1 */
  HOWTO (R_CKCORE_PCREL_IMM11BY2,	/* type */
	 1,			/* rightshift */
	 1,			/* size */
	 11,			/* bitsize */
	 TRUE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_signed, /* complain_on_overflow */
	 NULL,	/* special_function */
	 "R_CKCORE_PCREL_IMM11BY2",/* name */
	 FALSE,			/* partial_inplace */
	 0x7ff,			/* src_mask */
	 0x7ff,			/* dst_mask */
	 TRUE),			/* pcrel_offset */

  /* 4 */ /* DELETED */
  HOWTO (R_CKCORE_PCREL_IMM4BY2,0,0,0,0,0,0,0,"R_CKCORE_PCREL_IMM4BY2",0,0,0,0),
    
  /* 5 */
  HOWTO (R_CKCORE_PCREL32,/* type */
	 0,			/* rightshift */
	 2,			/* size */
	 32,			/* bitsize */
	 TRUE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_dont, /* complain_on_overflow */
	 NULL,	/* special_function */
	 "R_CKCORE_PCREL32",/* name */
	 FALSE,			/* partial_inplace */
	 0x0,			/* src_mask */
	 0xffffffff,			/* dst_mask */
	 TRUE),			/* pcrel_offset */

  /* 6 : only for V1 */
  HOWTO (R_CKCORE_PCREL_JSR_IMM11BY2,	/* type */
	 1,			/* rightshift */
	 1,			/* size */
	 11,			/* bitsize */
	 TRUE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_signed, /* complain_on_overflow */
	 NULL,	/* special_function */
	 "R_CKCORE_PCREL_JSR_IMM11BY2",/* name */
	 FALSE,			/* partial_inplace */
	 0x7ff,			/* src_mask */
	 0x7ff,			/* dst_mask */
	 TRUE),			/* pcrel_offset */

  /* 7 */ /* GNU extension to record C++ vtable member usage */
  HOWTO (R_CKCORE_GNU_VTENTRY,     /* type */
         0,                     /* rightshift */
         2,                     /* size */
         0,                     /* bitsize */
         FALSE,                 /* pc_relative */
         0,                     /* bitpos */
         complain_overflow_dont, /* complain_on_overflow */
         _bfd_elf_rel_vtable_reloc_fn,  /* special_function */
         "R_CKCORE_GNU_VTENTRY",   /* name */
         FALSE,                 /* partial_inplace */
         0,                     /* src_mask */
         0,                     /* dst_mask */
         FALSE),                /* pcrel_offset */

  /* 8 */ /* GNU extension to record C++ vtable hierarchy */
  HOWTO (R_CKCORE_GNU_VTINHERIT, /* type */
         0,                     /* rightshift */
         2,                     /* size */
         0,                     /* bitsize */
         FALSE,                 /* pc_relative */
         0,                     /* bitpos */
         complain_overflow_dont, /* complain_on_overflow */
         NULL,                  /* special_function */
         "R_CKCORE_GNU_VTINHERIT", /* name */
         FALSE,                 /* partial_inplace */
         0,                     /* src_mask */
         0,                     /* dst_mask */
         FALSE),                /* pcrel_offset */

  /* 9 */
  HOWTO (R_CKCORE_RELATIVE,
         0,                     /* rightshift */
         2,                     /* size */
         32,                     /* bitsize */
         FALSE,                 /* pc_relative */
         0,                     /* bitpos */
         complain_overflow_signed, /* complain_on_overflow */
         NULL,                  /* special_function */
         "R_CKCORE_RELATIVE",   /* name */
         TRUE,                 /* partial_inplace */
         0,                     /* src_mask */
         0xffffffff,            /* dst_mask */
         FALSE),                /* pcrel_offset */

  /* 10 */ /* NONE */
  HOWTO (R_CKCORE_COPY,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 11 */
  HOWTO (R_CKCORE_GLOB_DAT,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 12 */
  HOWTO (R_CKCORE_JUMP_SLOT,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 13 */
  HOWTO (R_CKCORE_GOTOFF,
         0,                     /* rightshift */
         2,                     /* size */
         32,                     /* bitsize */
         FALSE,                 /* pc_relative */
         0,                     /* bitpos */
         complain_overflow_dont, /* complain_on_overflow */
         bfd_elf_generic_reloc,      /* special_function */
         "R_CKCORE_GOTOFF",   /* name */
         TRUE,                 /* partial_inplace */
         0,                     /* src_mask */
         0xffffffff,            /* dst_mask */
         FALSE),                /* pcrel_offset */

  /* 14 */
  HOWTO (R_CKCORE_GOTPC,
         0,                     /* rightshift */
         2,                     /* size */
         32,                     /* bitsize */
         TRUE,                 /* pc_relative */
         0,                     /* bitpos */
         complain_overflow_dont, /* complain_on_overflow */
         bfd_elf_generic_reloc,      /* special_function */
         "R_CKCORE_GOTPC",   /* name */
         TRUE,                 /* partial_inplace */
         0,                     /* src_mask */
         0xffffffff,            /* dst_mask */
         FALSE),                /* pcrel_offset */

  /* 15 */
  HOWTO (R_CKCORE_GOT32,
         0,                     /* rightshift */
         2,                     /* size */
         32,                     /* bitsize */
         FALSE,                 /* pc_relative */
         0,                     /* bitpos */
         complain_overflow_dont, /* complain_on_overflow */
         bfd_elf_generic_reloc,      /* special_function */
         "R_CKCORE_GOT32",   /* name */
         TRUE,                 /* partial_inplace */
         0,                     /* src_mask */
         0xffffffff,            /* dst_mask */
         TRUE),                /* pcrel_offset */

  /* 16 */
  HOWTO (R_CKCORE_PLT32,
         0,                     /* rightshift */
         2,                     /* size */
         32,                     /* bitsize */
         FALSE,                 /* pc_relative */
         0,                     /* bitpos */
         complain_overflow_dont, /* complain_on_overflow */
         bfd_elf_generic_reloc,      /* special_function */
         "R_CKCORE_PLT32",     /* name */
         TRUE,                 /* partial_inplace */
         0,                     /* src_mask */
         0xffffffff,            /* dst_mask */
         TRUE),                /* pcrel_offset */

  /* 17 */
  HOWTO (R_CKCORE_ADDRGOT,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 18 */
  HOWTO (R_CKCORE_ADDRPLT,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 19 : only for V2 */
  HOWTO (R_CKCORE_PCREL_IMM26BY2,	/* type */
	 1,			/* rightshift */
	 2,			/* size */
	 26,			/* bitsize */
	 TRUE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_signed, /* complain_on_overflow */
	 NULL,/* special_function */
	 "R_CKCORE_PCREL_IMM26BY2",/* name */
	 FALSE,			/* partial_inplace */
	 0x3ffffff,			/* src_mask */
	 0x3ffffff,			/* dst_mask */
	 TRUE),			/* pcrel_offset */

  /* 20 : only for V2 */
  HOWTO (R_CKCORE_PCREL_IMM16BY2,	/* type */
	 1,			/* rightshift */
	 2,			/* size */
	 16,			/* bitsize */
	 TRUE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_signed, /* complain_on_overflow */
	 NULL,	/* special_function */
	 "R_CKCORE_PCREL_IMM16BY2",	/* name */
	 FALSE,			/* partial_inplace */
	 0xffff,			/* src_mask */
	 0xffff,		/* dst_mask */
	 TRUE),			/* pcrel_offset */

  /* 21 : only for V2 */
  HOWTO (R_CKCORE_PCREL_IMM16BY4,/* type */
	 2,			/* rightshift */
	 2,			/* size */
	 16,			/* bitsize */
	 TRUE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_bitfield, /* complain_on_overflow */
	 NULL,	/* special_function */
	 "R_CKCORE_PCREL_IMM16BY4", /* name */
	 FALSE,			/* partial_inplace */
	 0xffff,			/* src_mask */
	 0xffff,			/* dst_mask */
	 TRUE),			/* pcrel_offset */

  /* 22 : only for V2 */
  HOWTO (R_CKCORE_PCREL_IMM10BY2,      /* type */
	 1,			/* rightshift */
	 1,			/* size */
	 10,			/* bitsize */
	 TRUE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_signed, /* complain_on_overflow */
	 NULL,                  /* special_function */
	 "R_CKCORE_PCREL_IMM10BY2",    /* name */
	 FALSE,			/* partial_inplace */
	 0x3ff,		/* src_mask */
	 0x3ff,		/* dst_mask */
	 TRUE),			/* pcrel_offset */

  /* 23 : only for V2 */
  HOWTO (R_CKCORE_PCREL_IMM10BY4,      /* type */
	 2,			/* rightshift */
	 2,			/* size */
	 10,			/* bitsize */
	 TRUE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_bitfield, /* complain_on_overflow */
	 NULL,                  /* special_function */
	 "R_CKCORE_PCREL_IMM10BY4",    /* name */
	 FALSE,			/* partial_inplace */
	 0x3ff,		/* src_mask */
	 0x3ff,		/* dst_mask */
	 TRUE),			/* pcrel_offset */

  /* 24 */
  HOWTO (R_CKCORE_ADDR_HI16,      /* type */
	 16,			/* rightshift */
	 2,			/* size */
	 16,			/* bitsize */
	 FALSE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_dont, /* complain_on_overflow */
	 NULL,                  /* special_function */
	 "R_CKCORE_ADDR_HI16",    /* name */
	 FALSE,			/* partial_inplace */
	 0xffff,		/* src_mask */
	 0xffff,		/* dst_mask */
	 FALSE),			/* pcrel_offset */

  /* 25 */
  HOWTO (R_CKCORE_ADDR_LO16,      /* type */
	 0,			/* rightshift */
	 2,			/* size */
	 16,			/* bitsize */
	 FALSE,			/* pc_relative */
	 0,			/* bitpos */
	 complain_overflow_dont, /* complain_on_overflow */
	 NULL,                  /* special_function */
	 "R_CKCORE_ADDR_LO16",    /* name */
	 FALSE,			/* partial_inplace */
	 0xffff,		/* src_mask */
	 0xffff,		/* dst_mask */
	 FALSE),			/* pcrel_offset */

  /* 26 */
  HOWTO (R_CKCORE_GOTPC_HI16,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 27 */
  HOWTO (R_CKCORE_GOTPC_LO16,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 28 */
  HOWTO (R_CKCORE_GOTOFF_HI16,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 29 */
  HOWTO (R_CKCORE_GOTOFF_LO16,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 30 */
  HOWTO (R_CKCORE_GOT12,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 31 */
  HOWTO (R_CKCORE_GOT_HI16,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 32 */
  HOWTO (R_CKCORE_GOT_LO16,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 33 */
  HOWTO (R_CKCORE_PLT12,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 34 */
  HOWTO (R_CKCORE_PLT_HI16,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 35 */
  HOWTO (R_CKCORE_PLT_LO16,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 36 */
  HOWTO (R_CKCORE_ADDRGOT_HI16,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 37 */
  HOWTO (R_CKCORE_ADDRGOT_LO16,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 38 */
  HOWTO (R_CKCORE_ADDRPLT_HI16,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 39 */
  HOWTO (R_CKCORE_ADDRPLT_LO16,0,0,0,0,0,0,0,"R_CKCORE_",0,0,0,0),
  /* 40 */
  HOWTO (R_CKCORE_PCREL_JSR_IMM26BY2,    /* type */
         1,                     /* rightshift */
         2,                     /* size */
         26,                    /* bitsize */
         TRUE,                  /* pc_relative */
         0,                     /* bitpos */
         complain_overflow_signed, /* complain_on_overflow */
         NULL,  /* special_function */
         "R_CKCORE_PCREL_JSR_IMM26BY2",/* name */
         FALSE,                 /* partial_inplace */
         0x3ffffff,                 /* src_mask */
         0x3ffffff,                 /* dst_mask */
         TRUE)                  /* pcrel_offset */

};

/* NOTICE!
   The way how the following two look-up-functions work demand
   that BFD_RELOC_CKCORE_xxx are defined continuously. */
static reloc_howto_type *csky_elf_reloc_type_lookup
    (bfd * abfd ATTRIBUTE_UNUSED,bfd_reloc_code_real_type code)
{
    int csky_code = code - BFD_RELOC_CKCORE_NONE;
    
    if(csky_code < 0 || csky_code >= R_CKCORE_MAX)
    {
        switch (code)
        {
            case BFD_RELOC_NONE:             csky_code = R_CKCORE_NONE; break;
            case BFD_RELOC_32:               csky_code = R_CKCORE_ADDR32; break;
            case BFD_RELOC_32_PCREL:         csky_code = R_CKCORE_PCREL32; break;
            case BFD_RELOC_VTABLE_INHERIT:   csky_code = R_CKCORE_GNU_VTINHERIT; break;
            case BFD_RELOC_VTABLE_ENTRY:     csky_code = R_CKCORE_GNU_VTENTRY; break;
            case BFD_RELOC_RVA:              csky_code = R_CKCORE_RELATIVE; break;
            default:
                  return (reloc_howto_type *)NULL;
        }
    }
    // FIXME: here is dangerous, should be switch case all reloc type 
    return &csky_elf_howto_table[csky_code];
};

static reloc_howto_type *csky_elf_reloc_name_lookup
    (bfd *abfd ATTRIBUTE_UNUSED,const char *r_name)
{
    unsigned int i;
    for (i = 0; i < R_CKCORE_MAX; i++)
        if (strcasecmp (csky_elf_howto_table[i].name, r_name) == 0)
            return &csky_elf_howto_table[i];
    return NULL;
}
static void csky_elf_info_to_howto (
        bfd * abfd ATTRIBUTE_UNUSED,
        arelent * cache_ptr,
        Elf_Internal_Rela * dst)
{
    unsigned int r_type;
    r_type = ELF32_R_TYPE (dst->r_info);
    cache_ptr->howto = &csky_elf_howto_table [r_type];
}

// added by liub start

static struct bfd_link_hash_table *csky_elf_link_hash_table_create
  PARAMS ((bfd *));
static struct bfd_hash_entry * csky_elf_link_hash_newfunc
  PARAMS ((struct bfd_hash_entry *, struct bfd_hash_table *, const char *));

/* The name of the dynamic interpreter.  This is put in the .interp
   section.  */
#define ELF_DYNAMIC_INTERPRETER     "/usr/lib/ld.so.1"

/* The size in bytes of an entry in the procedure linkage table.  */
#define PLT_ENTRY_SIZE 12

/* The first entry in a procedure linkage table looks like
   this.  It is set up so that any shared library function that is
   called before the relocation has been set up calls the dynamic
   linker first.  */ 
static const bfd_vma csky_elf_plt_entry [PLT_ENTRY_SIZE / 2] =
  { 
    0x00000000, /* subi r0, 32        */
    0x00000000, /* stw r2, (r0, 0)    */
    0x00000000, /* stw r3, (r0, 4)    */
    0x00000000, /* ldw r2, (gb, 8)    */
    0x00000000, /* lrw r3, #offset    */
    0x00000000  /* jmp r2             */
  };

/* !! fixme The i386 linker needs to keep track of the number of relocs that it
   decides to copy as dynamic relocs in check_relocs for each symbol.
   This is so that it can later discard them if they are found to be
   unnecessary.  We store the information in a field extending the
   regular ELF linker hash table.  */

struct csky_elf_dyn_relocs
{ 
  struct csky_elf_dyn_relocs *next;
  
  /* The input section of the reloc.  */
  asection *sec;
  
  /* Total number of relocs copied for the input section.  */
  bfd_size_type count;
  
  /* Number of pc-relative relocs copied for the input section.  */
  bfd_size_type pc_count;
};


/* CKCORE ELF linker hash entry.  */
struct csky_elf_link_hash_entry
{
  struct elf_link_hash_entry elf;

  /* Track dynamic relocs copied for this symbol.  */
  struct csky_elf_dyn_relocs *dyn_relocs;

};

/* Traverse an CKCORE ELF linker hash table.  */
#define csky_elf_link_hash_traverse(table, func, info)                        \
  (elf_link_hash_traverse                                               \
   (&(table)->root,                                                     \
    (bfd_boolean (*) PARAMS ((struct elf_link_hash_entry *, PTR))) (func),  \
    (info)))

/* Get the CKCORE elf linker hash table from a link_info structure.  */
#define csky_elf_hash_table(info) \
  ((struct csky_elf_link_hash_table *) ((info)->hash))

#define csky_elf_hash_entry(ent)  ((struct csky_elf_link_hash_entry*)(ent))

/* CKCORE ELF linker hash table.  */
struct csky_elf_link_hash_table
{ 
  struct elf_link_hash_table elf;
  
  /* Short-cuts to get to dynamic linker sections.  */
  asection *sgot;
  asection *srelgot;
  asection *splt;
  
  /* Small local sym cache.  */
  struct sym_cache sym_cache;

};


/* Create an entry in an CKCORE ELF linker hash table.  */

static struct bfd_hash_entry *
csky_elf_link_hash_newfunc (entry, table, string)
     struct bfd_hash_entry * entry;
     struct bfd_hash_table * table;
     const char * string;
{
  /* Allocate the structure if it has not already been allocated by a
     subclass.  */
  if (entry == NULL)
    {
      entry = bfd_hash_allocate (table, 
                                 sizeof (struct csky_elf_link_hash_entry));
      if (entry == NULL)
        return entry;
    }
  
  /* Call the allocation method of the superclass.  */
  entry = _bfd_elf_link_hash_newfunc (entry, table, string);
  if(entry != NULL)
    {
      struct csky_elf_link_hash_entry *eh;

      eh = (struct csky_elf_link_hash_entry *) entry;
      eh->dyn_relocs = NULL;
    }
  
  return entry;
}


/* Create an CKCORE elf linker hash table.  */

static struct bfd_link_hash_table *
csky_elf_link_hash_table_create (abfd)
     bfd *abfd;
{

  struct csky_elf_link_hash_table *ret;
  bfd_size_type amt = sizeof (struct csky_elf_link_hash_table);

  ret = (struct csky_elf_link_hash_table*) bfd_malloc (amt);
  if (ret == NULL)
    return NULL;

  if (! _bfd_elf_link_hash_table_init (&ret->elf, abfd, 
                                       csky_elf_link_hash_newfunc,
                                       sizeof (struct csky_elf_link_hash_entry),
                                       CSKY_ELF_DATA))
    {
      free (ret);
      return NULL;
    }

  ret->sgot = NULL;
  ret->srelgot = NULL;
  ret->splt = NULL;
  ret->sym_cache.abfd = NULL;

  return &ret->elf.root;
}

/* Create .got, .gotplt, and .rel.got sections in DYNOBJ, and set up
   shortcuts to them in our hash table.  */

/* Create .plt, .rel.plt, .got, .got.plt, .rel.got, .dynbss, and
   .rel.bss sections in DYNOBJ, and set up shortcuts to them in our
   hash table.  */

static bfd_boolean csky_elf_create_dynamic_sections ( bfd *dynobj, 
     struct bfd_link_info *info);

static bfd_boolean
csky_elf_create_dynamic_sections (dynobj, info)
     bfd *dynobj;
     struct bfd_link_info *info;
{
  //struct csky_elf_link_hash_table *htab;

  if (!_bfd_elf_create_dynamic_sections (dynobj, info))
    return FALSE;

  //htab->splt = bfd_get_section_by_name (dynobj, ".plt");
  //htab->sgot = bfd_get_section_by_name (dynobj, ".got");
  //htab->srelgot = bfd_get_section_by_name (dynobj, ".rela.dyn");

  //if (!htab->splt || !htab->sgot || !htab->srelgot)
  //  abort ();

  return TRUE;
}


/* Adjust a symbol defined by a dynamic object and referenced by a
   regular object.  The current definition is in some section of the
   dynamic object, but we're not including those sections.  We have to
   change the definition to something the rest of the link can
   understand.  */

static bfd_boolean csky_elf_adjust_dynamic_symbol (
     struct bfd_link_info *info,
     struct elf_link_hash_entry *h);

static bfd_boolean
csky_elf_adjust_dynamic_symbol (info, h)
     struct bfd_link_info *info;
     struct elf_link_hash_entry *h;
{
    h->plt.offset = (bfd_vma) -1;

  /* If this is a weak symbol, and there is a real definition, the
     processor independent code will have arranged for us to see the
     real definition first, and we can just use the same value.  */
  if (h->u.weakdef != NULL)
    {
      BFD_ASSERT (h->u.weakdef->root.type == bfd_link_hash_defined
                  || h->u.weakdef->root.type == bfd_link_hash_defweak);
      h->root.u.def.section = h->u.weakdef->root.u.def.section;
      h->root.u.def.value = h->u.weakdef->root.u.def.value;
      return TRUE;
    }

  /* This is a reference to a symbol defined by a dynamic object which
     is not a function.  */

  /* If we are creating a shared library, we must presume that the
     only references to the symbol are via the global offset table.
     For such cases we need not do anything here; the relocations will
     be handled correctly by relocate_section.  */
  if (info->shared)
    return TRUE;

  h->non_got_ref = 0;
  return TRUE;
}


/* Allocate space in .plt, .got and associated reloc sections for
   dynamic relocs.  */
    
static bfd_boolean allocate_dynrelocs (
     struct elf_link_hash_entry *h,
     PTR inf);

static bfd_boolean
allocate_dynrelocs (h, inf)
     struct elf_link_hash_entry *h;
     PTR inf;
{ 
  struct bfd_link_info *info;
  struct csky_elf_link_hash_table *htab;
  struct csky_elf_link_hash_entry *eh;
  struct csky_elf_dyn_relocs *p;
  
  if (h->root.type == bfd_link_hash_indirect)
    return TRUE;
  
  if (h->root.type == bfd_link_hash_warning)
    /* When warning symbols are created, they **replace** the "real"
       entry in the hash table, thus we never get to see the real
       symbol in a hash traversal.  So look at it now.  */
    h = (struct elf_link_hash_entry *) h->root.u.i.link;
  
  info = (struct bfd_link_info *) inf;
  htab = csky_elf_hash_table (info);
  
  if (htab->elf.dynamic_sections_created
      && h->plt.refcount > 0)
    {
      /* Make sure this symbol is output as a dynamic symbol.
         Undefined weak syms won't yet be marked as dynamic.  */
      if (h->dynindx == -1
          && (!h->forced_local))
        {
          if (! bfd_elf_link_record_dynamic_symbol (info, h))
            return FALSE;
        }

      if (info->shared || WILL_CALL_FINISH_DYNAMIC_SYMBOL (1, 0, h))
        {
          asection *splt = htab->elf.splt;

          /* If this is the first .plt entry, make room for the special
             first entry.  */ 
          if (splt->size == 0)
            splt->size += PLT_ENTRY_SIZE;

          h->plt.offset = splt->size;

          /* If this symbol is not defined in a regular file, and we are
             not generating a shared library, then set the symbol to this
             location in the .plt.  This is required to make function
             pointers compare as equal between the normal executable and
             the shared library.  */
          if (! info->shared
              && !h->def_regular)
            {
              h->root.u.def.section = splt;
              h->root.u.def.value = h->plt.offset;
            }

          /* Make room for this entry.  */
          splt->size += PLT_ENTRY_SIZE;

        }
      else
        {
          h->plt.offset = (bfd_vma) -1;
          h->needs_plt = 0;
        }
    }
  else
    {
      h->plt.offset = (bfd_vma) -1;
      h->needs_plt = 0;
    }
  
  if (h->got.refcount > 0)
    {
      asection *sgot;
      bfd_boolean dyn;

      /* Make sure this symbol is output as a dynamic symbol.
         Undefined weak syms won't yet be marked as dynamic.  */
      if (h->dynindx == -1
          && (!h->forced_local))
        {
          if (! bfd_elf_link_record_dynamic_symbol (info, h))
            return FALSE;
        }

      sgot = htab->elf.sgot; 
      //if(sgot->size == 0) sgot->size += 3 * 4;
      h->got.offset = sgot->size;
      sgot->size += 4;
      dyn = htab->elf.dynamic_sections_created; 
      if ( (ELF_ST_VISIBILITY (h->other) == STV_DEFAULT
             || h->root.type != bfd_link_hash_undefweak) 
         && (info->shared 
             || WILL_CALL_FINISH_DYNAMIC_SYMBOL (dyn, 0, h)))
        htab->elf.srelgot->size += sizeof (Elf32_External_Rela);
    }
  else
    h->got.offset = (bfd_vma) -1;
  
  eh = (struct csky_elf_link_hash_entry *) h;
  if (eh->dyn_relocs == NULL)
    return TRUE;
  
  /* In the shared -Bsymbolic case, discard space allocated for
     dynamic pc-relative relocs against symbols which turn out to be
     defined in regular objects.  For the normal shared case, discard
     space for pc-relative relocs that have become local due to symbol
     visibility changes.  */
  
  if (info->shared)
    {
      if (SYMBOL_CALLS_LOCAL (info, h))
        {
          struct csky_elf_dyn_relocs **pp;

          for (pp = &eh->dyn_relocs; (p = *pp) != NULL; )
            {
              p->count -= p->pc_count;
              p->pc_count = 0;
              if (p->count == 0)
                *pp = p->next;
              else 
                pp = &p->next;
            }
        }

      /* Also discard relocs on undefined weak syms with non-default
         visibility.  */
      if (eh->dyn_relocs != NULL
          && h->root.type == bfd_link_hash_undefweak)
        {
          if (ELF_ST_VISIBILITY (h->other) != STV_DEFAULT)
            eh->dyn_relocs = NULL;

          /* Make sure undefined weak symbols are output as a dynamic
             symbol in PIEs.  */
          else if (h->dynindx == -1
                   && !h->forced_local)
            {
              if (! bfd_elf_link_record_dynamic_symbol (info, h))
                return FALSE;
            }
        } 

    }
  else
    {
      /* For the non-shared case, discard space for relocs against
         symbols which turn out to need copy relocs or are not
         dynamic.  */

      if ((!h->non_got_ref) 
          && (( h->def_dynamic 
               && !h->def_regular )
              || (htab->elf.dynamic_sections_created
                  && (h->root.type == bfd_link_hash_undefweak
                      || h->root.type == bfd_link_hash_undefined))))
        {
          /* Make sure this symbol is output as a dynamic symbol.
             Undefined weak syms won't yet be marked as dynamic.  */
          if (h->dynindx == -1
              && (!h->forced_local))
            {
              if (! bfd_elf_link_record_dynamic_symbol (info, h))
                return FALSE;
            }

          /* If that succeeded, we know we'll be keeping all the
             relocs.  */ 
          if (h->dynindx != -1)
            goto keep;
        }

      eh->dyn_relocs = NULL;

    keep: ;
    }
  
  /* Finally, allocate space.  */
  for (p = eh->dyn_relocs; p != NULL; p = p->next)
    {
      asection *srelgot = htab->elf.srelgot;
      srelgot->size += p->count * sizeof (Elf32_External_Rela);
    }
  
  return TRUE;
}

/* Find any dynamic relocs that apply to read-only sections.  */

static bfd_boolean readonly_dynrelocs (
     struct elf_link_hash_entry *h,
     PTR inf);

static bfd_boolean
readonly_dynrelocs (h, inf)
     struct elf_link_hash_entry *h;
     PTR inf;
{
  struct csky_elf_link_hash_entry *eh;
  struct csky_elf_dyn_relocs *p;

  if (h->root.type == bfd_link_hash_warning)
    h = (struct elf_link_hash_entry *) h->root.u.i.link;

  eh = (struct csky_elf_link_hash_entry *) h;
  for (p = eh->dyn_relocs; p != NULL; p = p->next)
    {
      asection *s = p->sec->output_section;

      if (s != NULL && (s->flags & SEC_READONLY) != 0)
        {
          struct bfd_link_info *info = (struct bfd_link_info *) inf;

          info->flags |= DF_TEXTREL;

          /* Not an error, just cut short the traversal.  */
          return FALSE;
        }
    }
  return TRUE;
}


/* Set the sizes of the dynamic sections.  */
static bfd_boolean csky_elf_size_dynamic_sections (
     bfd *output_bfd,
     struct bfd_link_info *info);

static bfd_boolean
csky_elf_size_dynamic_sections (output_bfd, info)
     bfd *output_bfd ATTRIBUTE_UNUSED;
     struct bfd_link_info *info;
{
  struct csky_elf_link_hash_table *htab;
  bfd *dynobj;
  asection *s;
  bfd_boolean relocs;
  bfd *ibfd;

  htab = csky_elf_hash_table (info);
  dynobj = htab->elf.dynobj;
  if (dynobj == NULL)
    abort ();

  if (htab->elf.dynamic_sections_created)
    {
      /* Set the contents of the .interp section to the interpreter.  */
      if (! info->shared)
        {
          s = bfd_get_section_by_name (dynobj, ".interp");
          if (s == NULL)
            abort ();
          s->size = sizeof ELF_DYNAMIC_INTERPRETER;
          s->contents = (unsigned char *) ELF_DYNAMIC_INTERPRETER;
        }
    }

  /* Set up .got offsets for local syms, and space for local dynamic
     relocs.  */
  for (ibfd = info->input_bfds; ibfd != NULL; ibfd = ibfd->link_next)
    {
      bfd_signed_vma *local_got_refcounts;
      bfd_signed_vma *end_local_got;
      bfd_size_type locsymcount;
      Elf_Internal_Shdr *symtab_hdr;
      asection *srelgot, *sgot; 
  
      if (bfd_get_flavour (ibfd) != bfd_target_elf_flavour)
        continue; 

      sgot = htab->elf.sgot;
      srelgot = htab->elf.srelgot;

      for (s = ibfd->sections; s != NULL; s = s->next)
        {
          struct csky_elf_dyn_relocs *p;

          for (p = *((struct csky_elf_dyn_relocs **)
                     &elf_section_data (s)->local_dynrel);
               p != NULL;
               p = p->next)
            {
              if (!bfd_is_abs_section (p->sec)
                  && bfd_is_abs_section (p->sec->output_section))
                {
                  /* Input section has been discarded, either because
                     it is a copy of a linkonce section or due to
                     linker script /DISCARD/, so we'll be discarding
                     the relocs too.  */
                }
              else if (p->count != 0)
                {
                  srelgot->size += p->count * sizeof (Elf32_External_Rela);
                  if ((p->sec->output_section->flags & SEC_READONLY) != 0)
                    info->flags |= DF_TEXTREL;
                }
            }
        }
  

      local_got_refcounts = elf_local_got_refcounts (ibfd);
      if (!local_got_refcounts)
        continue;

      symtab_hdr = &elf_tdata (ibfd)->symtab_hdr;
      locsymcount = symtab_hdr->sh_info;
      end_local_got = local_got_refcounts + locsymcount;
      for (; local_got_refcounts < end_local_got; ++local_got_refcounts)
        {
          if (*local_got_refcounts > 0)
            {
              //
              //if(sgot->size == 0) sgot->size += 3 * 4;
              *local_got_refcounts = sgot->size;
              sgot->size += 4;
              if (info->shared)
                srelgot->size += sizeof (Elf32_External_Rela);
            }
          else
            *local_got_refcounts = (bfd_vma) -1;
        }
    }
  
  /* Allocate global sym .plt and .got entries, and space for global
     sym dynamic relocs.  */
  elf_link_hash_traverse (&htab->elf, allocate_dynrelocs, (PTR) info);
  
  /* We now have determined the sizes of the various dynamic sections.
     Allocate memory for them.  */
  relocs = FALSE;
  for (s = dynobj->sections; s != NULL; s = s->next)
    {
      if ((s->flags & SEC_LINKER_CREATED) == 0)
        continue;

      if (s == htab->elf.splt || s == htab->elf.sgot) 
        {
        }
      else 
      if (strncmp (bfd_get_section_name (dynobj, s), ".rel", 4) == 0)
        {
          if (s->size != 0 )
            relocs = TRUE;

          /* We use the reloc_count field as a counter if we need
             to copy relocs into the output file.  */
          s->reloc_count = 0;
        }
      else
        {
          /* It's not one of our sections, so don't allocate space.  */
          continue;
        }

      /* Strip this section if we don't need it; see the
         comment below.  */
      if (s->size == 0)
        {
          /* If we don't need this section, strip it from the
             output file.  This is mostly to handle .rel.bss and
             .rel.plt.  We must create both sections in
             create_dynamic_sections, because they must be created
             before the linker maps input sections to output
             sections.  The linker does that before
             adjust_dynamic_symbol is called, and it is that
             function which decides whether anything needs to go
             into these sections.  */

          //_bfd_strip_section_from_output (info, s);
          s->flags |= SEC_EXCLUDE;
          continue;
        }

      if ((s->flags & SEC_HAS_CONTENTS) == 0)
        continue;


      /* Allocate memory for the section contents.  We use bfd_zalloc
         here in case unused entries are not reclaimed before the
         section's contents are written out.  This should not happen,
         but this way if it does, we get a R_386_NONE reloc instead
         of garbage.  */
      s->contents = (bfd_byte *) bfd_zalloc (dynobj, s->size);
      if (s->contents == NULL)
        return FALSE;
    }

  if (htab->elf.dynamic_sections_created)
    {
      /* Add some entries to the .dynamic section.  We fill in the
         values later, in csky_elf_finish_dynamic_sections, but we
         must add the entries now so that we get the correct size for
         the .dynamic section.  The DT_DEBUG entry is filled in by the
         dynamic linker and used by the debugger.  */
#define add_dynamic_entry(TAG, VAL) \
  _bfd_elf_add_dynamic_entry(info, TAG, VAL)

      if (! info->shared)
        {
          if (!add_dynamic_entry (DT_DEBUG, 0))
            return FALSE;
        }

      if (htab->elf.sgot->size != 0 || htab->elf.splt->size)
        {
          if (!add_dynamic_entry (DT_PLTGOT, 0)
              || !add_dynamic_entry (DT_PLTRELSZ, 0)
              || !add_dynamic_entry (DT_PLTREL, DT_RELA)
              || !add_dynamic_entry (DT_JMPREL, 0))
            return FALSE;
        }

      if (relocs)
        {
          if (!add_dynamic_entry (DT_RELA, 0)
              || !add_dynamic_entry (DT_RELASZ, 0)
              || !add_dynamic_entry (DT_RELAENT, sizeof (Elf32_External_Rela)))
            return FALSE;

          /* If any dynamic relocs apply to a read-only section,
             then we need a DT_TEXTREL entry.  */
          if ((info->flags & DF_TEXTREL) == 0)
            elf_link_hash_traverse (&htab->elf, readonly_dynrelocs,
                                    (PTR) info);

          if ((info->flags & DF_TEXTREL) != 0)
            {
              if (!add_dynamic_entry (DT_TEXTREL, 0))
                return FALSE;
            }
        }
    }
#undef add_dynamic_entry

  return TRUE;
}



/* Finish up dynamic symbol handling.  We set the contents of various
   dynamic sections here.  */

static bfd_boolean csky_elf_finish_dynamic_symbol (
     bfd *output_bfd,
     struct bfd_link_info *info,
     struct elf_link_hash_entry *h,
     Elf_Internal_Sym *sym);

static bfd_boolean
csky_elf_finish_dynamic_symbol (output_bfd, info, h, sym)
     bfd *output_bfd;
     struct bfd_link_info *info;
     struct elf_link_hash_entry *h;
     Elf_Internal_Sym *sym;
{   
  struct csky_elf_link_hash_table *htab;

  htab = csky_elf_hash_table (info);

  if (h->got.offset != (bfd_vma) -1)
    {
      Elf_Internal_Rela rel;
      bfd_byte *loc;

      /* This symbol has an entry in the global offset table.  Set it
         up.  */

      if (htab->elf.sgot == NULL || htab->elf.srelgot == NULL )
        abort ();

      rel.r_offset = (htab->elf.sgot->output_section->vma
                      + htab->elf.sgot->output_offset
                      + (h->got.offset & ~(bfd_vma) 1));

      /* If this is a static link, or it is a -Bsymbolic link and the
         symbol is defined locally or was forced to be local because
         of a version file, we just want to emit a RELATIVE reloc.
         The entry in the global offset table will already have been
         initialized in the relocate_section function.  */
      if (info->shared
          && SYMBOL_REFERENCES_LOCAL (info, h))
        {
          BFD_ASSERT((h->got.offset & 1) != 0);
          rel.r_info = ELF32_R_INFO (0, R_CKCORE_RELATIVE);
          rel.r_addend = (h->root.u.def.value
                          + h->root.u.def.section->output_offset
                          + h->root.u.def.section->output_section->vma);
        }
      else
        {
          BFD_ASSERT((h->got.offset & 1) == 0);
          bfd_put_32 (output_bfd, (bfd_vma) 0,
                      htab->elf.sgot->contents + h->got.offset);
          rel.r_info = ELF32_R_INFO (h->dynindx, R_CKCORE_GLOB_DAT);
          rel.r_addend = 0;
        }

      loc = htab->elf.srelgot->contents;
      loc += htab->elf.srelgot->reloc_count++ * sizeof (Elf32_External_Rela);
      bfd_elf32_swap_reloca_out (output_bfd, &rel, loc);
    }

  if (h->needs_copy)
    {
    }

  /* Mark _DYNAMIC and _GLOBAL_OFFSET_TABLE_ as absolute.  */
  if (strcmp (h->root.root.string, "_DYNAMIC") == 0
      || strcmp (h->root.root.string, "_GLOBAL_OFFSET_TABLE_") == 0)
    sym->st_shndx = SHN_ABS;

  return TRUE;
}

/* Used to decide how to sort relocs in an optimal manner for the
   dynamic linker, before writing them out.  */

static enum elf_reloc_type_class csky_elf_reloc_type_class (
     const Elf_Internal_Rela *rela);

static enum elf_reloc_type_class
csky_elf_reloc_type_class (rela)
     const Elf_Internal_Rela *rela;
{
  switch ((int) ELF32_R_TYPE (rela->r_info))
    {
    case R_CKCORE_RELATIVE:
      return reloc_class_relative;
    case R_CKCORE_JUMP_SLOT:
      return reloc_class_plt;
    case R_CKCORE_COPY:
      return reloc_class_copy;
    default:
      return reloc_class_normal;
    }
}


/* Finish up the dynamic sections.  */

static bfd_boolean csky_elf_finish_dynamic_sections(
     bfd *output_bfd,
     struct bfd_link_info *info);

static bfd_boolean
csky_elf_finish_dynamic_sections(output_bfd, info)
     bfd *output_bfd;
     struct bfd_link_info *info;
{
  struct csky_elf_link_hash_table *htab;
  bfd *dynobj;
  asection *sdyn;

  htab = csky_elf_hash_table (info);
  dynobj = htab->elf.dynobj;
  sdyn = bfd_get_section_by_name (dynobj, ".dynamic");

  if (htab->elf.dynamic_sections_created)
    {
      Elf32_External_Dyn *dyncon, *dynconend;

      if (sdyn == NULL || htab->elf.sgot == NULL)
        abort ();

      dyncon = (Elf32_External_Dyn *) sdyn->contents;
      dynconend = (Elf32_External_Dyn *) (sdyn->contents + sdyn->size);
      for (; dyncon < dynconend; dyncon++)
        {
          Elf_Internal_Dyn dyn;
          bfd_boolean size = FALSE;
          const char *name = NULL;

          bfd_elf32_swap_dyn_in (dynobj, dyncon, &dyn);
  
          switch (dyn.d_tag)
            {
            default:
              continue;
            case DT_RELA:      name = ".rela.dyn"; size = FALSE;  break;
            case DT_RELASZ:    name = ".rela.dyn"; size = TRUE;  break;
            case DT_PLTGOT:
              dyn.d_un.d_ptr = htab->elf.sgot->output_section->vma;
              break;

            }

          if (name != NULL)
            {
              asection *s;
    
              s = bfd_get_section_by_name (output_bfd, name);
              if (s == NULL)
                dyn.d_un.d_val = 0;
              else
                {
                  if (! size)
                    dyn.d_un.d_ptr = s->vma;
                  else
                    {
                      if (s->size != 0)
                        dyn.d_un.d_val = s->size;
                      else
                        dyn.d_un.d_val = s->size;
                    }
                }
            }
          bfd_elf32_swap_dyn_out (output_bfd, &dyn, dyncon);
        }

    }

  if (htab->elf.sgot)
    {
      /* Fill in the first three entries in the global offset table.  */
      if (htab->elf.sgot->size > 0)
        {
          bfd_put_32 (output_bfd,
                      (sdyn == NULL ? (bfd_vma) 0
                       : sdyn->output_section->vma + sdyn->output_offset),
                      htab->elf.sgot->contents);
          bfd_put_32 (output_bfd, (bfd_vma) 0, htab->elf.sgot->contents + 4);
          bfd_put_32 (output_bfd, (bfd_vma) 0, htab->elf.sgot->contents + 8);
        }

      elf_section_data (htab->elf.sgot->output_section)->this_hdr.sh_entsize = 4;
    }
  return TRUE;

}


// added by liub end


/* We can't change vectors in the bfd target which will apply to 
   data sections, however we only do this to the text sections. */
static bfd_vma _csky_get_insn_32(bfd *input_bfd, bfd_byte *location)
{
    if(bfd_big_endian(input_bfd))
        return bfd_get_32 (input_bfd, location);
    else return bfd_get_16(input_bfd, location) << 16 
        | bfd_get_16(input_bfd, location + 2);
}
static void _csky_put_insn_32(bfd *input_bfd, bfd_vma x, bfd_byte *location)
{
    if(bfd_big_endian(input_bfd))
        bfd_put_32 (input_bfd, x, location);
    else
    {
        bfd_put_16 (input_bfd, x>>16, location);
        bfd_put_16 (input_bfd, x&0xffff, location+2);
    }
}

/* whether the target 32bits is forced to be that the high
   16bits is at the low address */
static int need_reverse_bits;
static bfd_vma read_content_substitute;

static bfd_reloc_status_type _csky_relocate_contents (
            reloc_howto_type *howto,
			bfd *input_bfd,
			bfd_vma relocation,
			bfd_byte *location)
{
  int size;
  bfd_vma x = 0;
  bfd_reloc_status_type flag;
  unsigned int rightshift = howto->rightshift;
  unsigned int bitpos = howto->bitpos;

  /* If the size is negative, negate RELOCATION.  This isn't very
     general.  */
  if (howto->size < 0)
    relocation = -relocation;

  /* Get the value we are going to relocate.  */
  size = bfd_get_reloc_size (howto);
  switch (size)
    {
    default:
    case 0:
      abort ();
    case 1:
      x = bfd_get_8 (input_bfd, location);
      break;
    case 2:
      if(read_content_substitute)
        x = read_content_substitute;
      else 
        x = bfd_get_16 (input_bfd, location);
      break;
    case 4:
      if(read_content_substitute)
          x = read_content_substitute;
      else
      {
          if(need_reverse_bits)
              x = _csky_get_insn_32 (input_bfd, location);
          else
              x = bfd_get_32 (input_bfd, location);
      }
      break;
    case 8:
#ifdef BFD64
      x = bfd_get_64 (input_bfd, location);
#else
      abort ();
#endif
      break;
    }

  /* Check for overflow.  FIXME: We may drop bits during the addition
     which we don't check for.  We must either check at every single
     operation, which would be tedious, or we must do the computations
     in a type larger than bfd_vma, which would be inefficient.  */
  flag = bfd_reloc_ok;
  if (howto->complain_on_overflow != complain_overflow_dont)
    {
      int addrmask, fieldmask, signmask, ss;
      int a, b, sum;
      /* Get the values to be added together.  For signed and unsigned
         relocations, we assume that all values should be truncated to
         the size of an address.  For bitfields, all the bits matter.
         See also bfd_check_overflow.  */
#define N_ONES(n) (((((bfd_vma) 1 << ((n) - 1)) - 1) << 1) | 1)
      fieldmask = N_ONES (howto->bitsize);
      signmask = ~fieldmask;
      addrmask = N_ONES (bfd_arch_bits_per_address (input_bfd)) | fieldmask;
      a = (relocation & addrmask) >> rightshift;
      b = (x & howto->src_mask & addrmask) >> bitpos;

      switch (howto->complain_on_overflow)
	{
	case complain_overflow_signed:
	  /* If any sign bits are set, all sign bits must be set.
	     That is, A must be a valid negative address after
	     shifting.  */
	  signmask = ~(fieldmask >> 1);
	  /* Fall thru */

	case complain_overflow_bitfield:
	  /* Much like the signed check, but for a field one bit
	     wider.  We allow a bitfield to represent numbers in the
	     range -2**n to 2**n-1, where n is the number of bits in the
	     field.  Note that when bfd_vma is 32 bits, a 32-bit reloc
	     can't overflow, which is exactly what we want.  */
	  ss = a & signmask;
	  if (ss != 0 && ss != ((addrmask >> rightshift) & signmask))
	    flag = bfd_reloc_overflow;

	  /* We only need this next bit of code if the sign bit of B
             is below the sign bit of A.  This would only happen if
             SRC_MASK had fewer bits than BITSIZE.  Note that if
             SRC_MASK has more bits than BITSIZE, we can get into
             trouble; we would need to verify that B is in range, as
             we do for A above.  */
	  ss = ((~howto->src_mask) >> 1) & howto->src_mask;
	  ss >>= bitpos;

	  /* Set all the bits above the sign bit.  */
	  b = (b ^ ss) - ss;

	  /* Now we can do the addition.  */
	  sum = a + b;

	  /* See if the result has the correct sign.  Bits above the
             sign bit are junk now; ignore them.  If the sum is
             positive, make sure we did not have all negative inputs;
             if the sum is negative, make sure we did not have all
             positive inputs.  The test below looks only at the sign
             bits, and it really just
	         SIGN (A) == SIGN (B) && SIGN (A) != SIGN (SUM)

	     We mask with addrmask here to explicitly allow an address
	     wrap-around.  The Linux kernel relies on it, and it is
	     the only way to write assembler code which can run when
	     loaded at a location 0x80000000 away from the location at
	     which it is linked.  */
	  if (((~(a ^ b)) & (a ^ sum)) & signmask & addrmask)
	    flag = bfd_reloc_overflow;
	  break;

	case complain_overflow_unsigned:
	  /* Checking for an unsigned overflow is relatively easy:
             trim the addresses and add, and trim the result as well.
             Overflow is normally indicated when the result does not
             fit in the field.  However, we also need to consider the
             case when, e.g., fieldmask is 0x7fffffff or smaller, an
             input is 0x80000000, and bfd_vma is only 32 bits; then we
             will get sum == 0, but there is an overflow, since the
             inputs did not fit in the field.  Instead of doing a
             separate test, we can check for this by or-ing in the
             operands when testing for the sum overflowing its final
             field.  */
	  sum = (a + b) & addrmask;
	  if ((a | b | sum) & signmask)
	    flag = bfd_reloc_overflow;
	  break;

	default:
	  abort ();
	}
    }

  /* Put RELOCATION in the right bits.  */
  relocation >>= (bfd_vma) rightshift;

  /* insn V1, all this relocation must be x-1 */
  if((howto->type == R_CKCORE_PCREL_IMM11BY2) ||
     (howto->type == R_CKCORE_PCREL_JSR_IMM11BY2) ||
     (howto->type == R_CKCORE_PCREL_IMM8BY4))
  {
    relocation -= 1;
  }

  // set to the position 
  relocation <<= (bfd_vma) bitpos;

  /* Add RELOCATION to the right bits of X.  */
  x = ((x & ~howto->dst_mask)
       | (((x & howto->src_mask) + relocation) & howto->dst_mask));

  /* Put the relocated value back in the object file.  */
  switch (size)
    {
    default:
      abort ();
    case 1:
      bfd_put_8 (input_bfd, x, location);
      break;
    case 2:
      bfd_put_16 (input_bfd, x, location);
      break;
    case 4:
      if(need_reverse_bits)
          _csky_put_insn_32(input_bfd, x, location);
      else
          bfd_put_32(input_bfd, x, location);
      break;
    case 8:
#ifdef BFD64
      bfd_put_64 (input_bfd, x, location);
#else
      abort ();
#endif
      break;
    }

  return flag;
}


static bfd_reloc_status_type _csky_final_link_relocate (
    reloc_howto_type *howto, bfd *input_bfd, asection *input_section,
    bfd_byte *contents, bfd_vma address, bfd_vma value, bfd_vma addend)
{
  bfd_vma relocation;

  /* Sanity check the address.  */
  if (address > bfd_get_section_limit (input_bfd, input_section))
    return bfd_reloc_outofrange;

  /* This function assumes that we are dealing with a basic relocation
     against a symbol.  We want to compute the value of the symbol to
     relocate to.  This is just VALUE, the value of the symbol, plus
     ADDEND, any addend associated with the reloc.  */
  relocation = value + addend;

  /* If the relocation is PC relative, we want to set RELOCATION to
     the distance between the symbol (currently in RELOCATION) and the
     location we are relocating.  Some targets (e.g., i386-aout)
     arrange for the contents of the section to be the negative of the
     offset of the location within the section; for such targets
     pcrel_offset is FALSE.  Other targets (e.g., m88kbcs or ELF)
     simply leave the contents of the section as zero; for such
     targets pcrel_offset is TRUE.  If pcrel_offset is FALSE we do not
     need to subtract out the offset of the location within the
     section (which is just ADDRESS).  */
  if (howto->pc_relative)
    {
      relocation -= (input_section->output_section->vma
		     + input_section->output_offset);
      if (howto->pcrel_offset)
	relocation -= address;
    }

  return _csky_relocate_contents (howto, input_bfd, relocation,
				 contents + address);
}
/*
static void _csky_special_final_link_relocate()
{
    return;
}
*/
static bfd_boolean csky_elf_relocate_section (
        bfd * output_bfd,
        struct bfd_link_info * info,
        bfd * input_bfd,
        asection * input_section,
        bfd_byte * contents,
        Elf_Internal_Rela * relocs,
        Elf_Internal_Sym * local_syms,
        asection ** local_sections)
{
    Elf_Internal_Shdr *                symtab_hdr = & elf_tdata (input_bfd)->symtab_hdr;
    struct elf_link_hash_entry **      sym_hashes = elf_sym_hashes (input_bfd);
    struct csky_elf_link_hash_table* htab = csky_elf_hash_table (info);
    bfd_vma *                          local_got_offsets = elf_local_got_offsets (input_bfd);
    Elf_Internal_Rela *                rel = relocs;
    Elf_Internal_Rela *                relend = relocs + input_section->reloc_count;
    bfd_boolean ret = TRUE;

#ifdef DEBUG
  _bfd_error_handler
    ("csky_elf_relocate_section called for %B section %A, %ld relocations%s",
     input_bfd,
     input_section,
     (long) input_section->reloc_count,
     (info->relocatable) ? " (relocatable)" : "");
#endif

    for (; rel < relend; rel++)
    {
        enum elf_csky_reloc_type     r_type = (enum elf_csky_reloc_type) ELF32_R_TYPE (rel->r_info);
        bfd_vma                      off;
        bfd_vma                      addend = rel->r_addend;
        bfd_reloc_status_type        r = bfd_reloc_ok;
        asection *                   sec = NULL;
        reloc_howto_type *           howto;
        bfd_vma                      relocation;
        Elf_Internal_Sym *           sym = NULL;
        unsigned long                r_symndx;
        struct elf_link_hash_entry * h = NULL;
        bfd_boolean                  unresolved_reloc = FALSE;


        if (r_type == R_CKCORE_GNU_VTINHERIT
            || r_type == R_CKCORE_GNU_VTENTRY)
          continue;

        if ((unsigned) r_type >= (unsigned) R_CKCORE_MAX)  // FIXME: or howto table not implemented 
        {
            _bfd_error_handler (_("%B: Unknown relocation type %d\n"),
                      input_bfd, (int) r_type);
            bfd_set_error (bfd_error_bad_value);
            ret = FALSE;
            continue;
        }
  
        howto = &csky_elf_howto_table [(int) r_type];

        /* Complain about known relocation that are not yet supported.  */
//        if (howto->special_function == 0 ) //csky_elf_unsupported_reloc)
//        {
//          _bfd_error_handler (_("%B: Relocation %s (%d) is not currently supported.\n"),
//                              (input_bfd),
//                              howto->name,
//                              (int)r_type);
//
//          bfd_set_error (bfd_error_bad_value);
//          ret = FALSE;
//          continue;
//        }

        r_symndx = ELF32_R_SYM (rel->r_info);
        h = NULL;
        sym = NULL;
        sec = NULL;
        unresolved_reloc = FALSE;
        /* relocate */
        if (r_symndx < symtab_hdr->sh_info)
        {
            sym = local_syms + r_symndx;
            sec = local_sections [r_symndx];
            relocation = _bfd_elf_rela_local_sym (output_bfd, sym, &sec, rel);
            addend = rel->r_addend;
        }
        else
        {
            bfd_boolean warned;

            RELOC_FOR_GLOBAL_SYMBOL (info, input_bfd, input_section, rel,
               r_symndx, symtab_hdr, sym_hashes,
               h, sec, relocation,
               unresolved_reloc, warned);
        }

        if (sec != NULL && elf_discarded_section (sec))
        {
            /* For relocs against symbols from removed linkonce sections,
            or sections discarded by a linker script, we just want the
            section contents zeroed.  Avoid any special processing.  */
            _bfd_clear_contents (howto, input_bfd, contents + rel->r_offset);
            rel->r_info = 0;
            rel->r_addend = 0;
            continue;
        }
        
        if (info->relocatable) continue;

#ifdef DEBUG
      fprintf (stderr, "\ttype = %s (%d), symbol index = %ld, offset = %ld, addend = %ld\n",
	       howto->name, r_type, r_symndx, (long) rel->r_offset, (long) addend);
#endif

        /* final link */
        long disp = relocation /* value  */
                + (long)addend     /* addend */
                - input_section->output_section->vma
                - input_section->output_offset
                - rel->r_offset;
        #define CSKY_INSN_BSR32 0x04000000    // for 8xx
        #define CSKY_INSN_BSR16 0xf800        // for 5xx
        #define within_range(x,L) (-(1 << (L - 1)) < (x) && (x) < (1 << (L - 1)) - 2)
        switch(howto->type)
        {
            case R_CKCORE_GOT32:
            case R_CKCORE_PLT32: // do plt32 same as got32, no lazy load 
                /* Relocation is to the entry for this symbol in the global
                   offset table.  */
                if (htab->elf.sgot == NULL)
                  abort ();
      
                if (h != NULL)   // global symbola defined by other modules
                  {
                    bfd_boolean dyn;
      
                    off = h->got.offset;
                    dyn = htab->elf.dynamic_sections_created;
                    if (! WILL_CALL_FINISH_DYNAMIC_SYMBOL (dyn, info->shared, h)
                        || (info->shared
                            && SYMBOL_REFERENCES_LOCAL (info, h))
                        || (ELF_ST_VISIBILITY (h->other)
                            && h->root.type == bfd_link_hash_undefweak))
                      {
                        /* This is actually a static link, or it is a
                           -Bsymbolic link and the symbol is defined
                           locally, or the symbol was forced to be local
                           because of a version file.  We must initialize
                           this entry in the global offset table.  Since the
                           offset must always be a multiple of 4, we use the
                           least significant bit to record whether we have
                           initialized it already.
      
                           When doing a dynamic link, we create a .rela.dyn
                           relocation entry to initialize the value.  This
                           is done in the finish_dynamic_symbol routine. FIXME */
                        if ((off & 1) != 0)
                          off &= ~1;
                        else
                          {
                            bfd_put_32 (output_bfd, relocation,
                                        htab->elf.sgot->contents + off);
                            h->got.offset |= 1;
                          }
                      }
                    else
                      unresolved_reloc = FALSE;
                  }
                else
                  {
                    if (local_got_offsets == NULL)
                      abort ();
      
                    off = local_got_offsets[r_symndx];
      
                    /* The offset must always be a multiple of 4.  We use
                       the least significant bit to record whether we have
                       already generated the necessary reloc.  */
                    if ((off & 1) != 0)
                      off &= ~1;
                    else
                      {
                      //  bfd_put_32 (output_bfd, relocation,
                      //              htab->elf.sgot->contents + off);
      
                        if (info->shared)
                          {
                            asection *srelgot;
                            Elf_Internal_Rela outrel;
                            bfd_byte *loc;
      
                            srelgot = htab->elf.srelgot;
                            if (srelgot == NULL)
                              abort ();
      
                            outrel.r_offset = (htab->elf.sgot->output_section->vma
                                               + htab->elf.sgot->output_offset
                                               + off);
                            outrel.r_info = ELF32_R_INFO (0, R_CKCORE_RELATIVE);
                            outrel.r_addend = relocation;  // FIXME
                            loc = srelgot->contents;
                            loc += srelgot->reloc_count++ * sizeof(Elf32_External_Rela);
                            bfd_elf32_swap_reloca_out (output_bfd, &outrel, loc);
                          }
      
                        local_got_offsets[r_symndx] |= 1;
                      }
                  }
      
                // FIXME
                //if (off >= (bfd_vma) -2)
                //  abort ();
      
                relocation = htab->elf.sgot->output_offset + off;
                break;


            case R_CKCORE_GOTOFF:
              /* Relocation is relative to the start of the global offset
                 table.  */
    
              /* Note that sgot->output_offset is not involved in this
                 calculation.  We always want the start of .got.  If we
                 defined _GLOBAL_OFFSET_TABLE in a different way, as is
                 permitted by the ABI, we might have to change this
                 calculation.  */
              relocation -= htab->elf.sgot->output_section->vma;
              break;
    
            case R_CKCORE_GOTPC:
              /* Use global offset table as symbol value.  */
              relocation = htab->elf.sgot->output_section->vma;
              addend = -addend;
              unresolved_reloc = FALSE;
              break;
    
            //case R_CKCORE_PLT32:
            //  /* Relocation is to the entry for this symbol in the
            //     procedure linkage table.  */
    
            //  /* Resolve a PLT32 reloc against a local symbol directly,
            //     without using the procedure linkage table.  */
            //  if (h == NULL)
            //    break;
    
            //  if (h->plt.offset == (bfd_vma) -1
            //      || htab->splt == NULL)
            //    {
            //      /* We didn't make a PLT entry for this symbol.  This
            //       happens when statically linking PIC code, or when
            //       using -Bsymbolic.  */
            //      break;
            //    }
    
            //  relocation = (htab->splt->output_section->vma
            //              + htab->splt->output_offset
            //              + h->plt.offset);
            //  unresolved_reloc = FALSE;
            //  break;
    
            case R_CKCORE_ADDR32:
              /* r_symndx will be zero only for relocs against symbols
                 from removed linkonce sections, or sections discarded by
                 a linker script.  */
              if (r_symndx == 0
                  || (input_section->flags & SEC_ALLOC) == 0)
                break;
    
              if ((info->shared)
                  || (!info->shared
                      && h != NULL
                      && h->dynindx != -1
                      && !h->non_got_ref
                      && ((h->def_dynamic
                           && !h->def_regular)
                          || h->root.type == bfd_link_hash_undefweak
                          || h->root.type == bfd_link_hash_undefined)))
                {
                  Elf_Internal_Rela outrel;
                  bfd_boolean skip, relocate;
                  bfd_byte *loc;
    
                  /* When generating a shared object, these relocations
                     are copied into the output file to be resolved at run
                     time.  */
    
                  skip = FALSE;
                  relocate = FALSE;
    
                  outrel.r_offset =
                    _bfd_elf_section_offset (output_bfd, info, input_section,
                                             rel->r_offset);
                  if (outrel.r_offset == (bfd_vma) -1)
                    skip = TRUE;
                  else if (outrel.r_offset == (bfd_vma) -2)
                    skip = TRUE, relocate = TRUE;
                  outrel.r_offset += (input_section->output_section->vma
                                      + input_section->output_offset);
    
                  if (skip)
                    memset (&outrel, 0, sizeof (outrel));
                  else if (h != NULL
                           && h->dynindx != -1
                           && (!info->shared
                               || !SYMBOLIC_BIND (info, h) 
                               ||  !h->def_regular))
                    {
                      outrel.r_info = ELF32_R_INFO (h->dynindx, r_type);
                      outrel.r_addend = rel->r_addend;
                    }
                  else
                    {
                      /* This symbol is local, or marked to become local.  */
                      relocate = TRUE;
                      outrel.r_info = ELF32_R_INFO (0, R_CKCORE_RELATIVE);
                      outrel.r_addend = relocation + rel->r_addend;
                    }
    
                  loc = htab->elf.srelgot->contents;
                  loc += htab->elf.srelgot->reloc_count++ * sizeof(Elf32_External_Rela);
                  bfd_elf32_swap_reloca_out (output_bfd, &outrel, loc);
    
                  /* If this reloc is against an external symbol, we do
                     not want to fiddle with the addend.  Otherwise, we
                     need to include the symbol value so that it becomes
                     an addend for the dynamic reloc.  */
                  if (! relocate)
                    continue;
                }
    
              break;


            case R_CKCORE_PCREL_JSR_IMM11BY2:
                /* If 'jsri' is within certain range, it will be changed
                   into 'bsr' */
                if(within_range(disp,11))
                {
                    /* change the data that in memory for processing,
                       and will not apply the these to the object file. */
                    howto=&csky_elf_howto_table[R_CKCORE_PCREL_IMM11BY2];
                    read_content_substitute = CSKY_INSN_BSR16;
                }
                else
                {
                    /* no subtitution when final linking */
                    read_content_substitute = 0;
                }
                break;
            case R_CKCORE_PCREL_JSR_IMM26BY2:
                /* If 'jsri' is within certain range, it will be changed
                   into 'bsr' */
                if(within_range(disp,26))
                {
                    /* change the data that in memory for processing,
                       and will not apply the these to the object file. */
                    howto=&csky_elf_howto_table[R_CKCORE_PCREL_IMM26BY2];
                    read_content_substitute = CSKY_INSN_BSR32;
                }
                else
                {
                    /* no subtitution when final linking */
                    read_content_substitute = 0;
                }
                break;
            default:
                /* no subtitution when final linking */
                read_content_substitute = 0;
        }

        /* Make sure a 32bit data in the text section will not
           be affect by out special endianess. */
        /* However, this currently affects nothing, since the ADDR32 
           howto type does no change with the data read. But
           We may need this mechanism in the future. */
        if(howto->size==2 
           &&  (howto->type==R_CKCORE_ADDR32
               || howto->type == R_CKCORE_GOT32
               || howto->type == R_CKCORE_GOTOFF
               || howto->type == R_CKCORE_GOTPC
               || howto->type == R_CKCORE_PLT32
               || howto->type == R_CKCORE_RELATIVE))
            need_reverse_bits=0;
        else need_reverse_bits=1;
        
        /* Do the final link. */
        if((howto->type != R_CKCORE_PCREL_JSR_IMM11BY2) &&
           (howto->type != R_CKCORE_PCREL_JSR_IMM26BY2) )
        {  // ignore  _JSR_IMMxxx
            r = _csky_final_link_relocate
                (howto, input_bfd, input_section, contents, rel->r_offset, relocation, addend);
        }

        const char * name;
        if (r != bfd_reloc_ok) 
        {
            ret = FALSE;

            switch (r)
            {
                default:
                    break;
                case bfd_reloc_overflow:

                    if (h != NULL)
                        name = NULL;
                    else
                    {
                        name = bfd_elf_string_from_elf_section
                          (input_bfd, symtab_hdr->sh_link, sym->st_name);

                        if (name == NULL)
                          break;

                        if (* name == '\0')
                          name = bfd_section_name (input_bfd, sec);
                    }

                    (*info->callbacks->reloc_overflow)
                    (info, (h ? &h->root : NULL), name, howto->name,
                    (bfd_vma) 0, input_bfd, input_section, rel->r_offset);
                    break;
            }
        }
    }
    
    #ifdef DEBUG
    fprintf (stderr, "\n");
    #endif
    
    return ret;
}

/* Return the section that should be marked against GC for a given
   relocation.  */
static asection *csky_elf_gc_mark_hook (asection *sec,
			struct bfd_link_info *info,
			Elf_Internal_Rela *rel,
			struct elf_link_hash_entry *h,
			Elf_Internal_Sym *sym)
{
  ASSERTLINE;
  if (h != NULL)
    switch (ELF32_R_TYPE (rel->r_info))
      {
      case R_CKCORE_GNU_VTINHERIT:
      case R_CKCORE_GNU_VTENTRY:
	return NULL;
      }

  return _bfd_elf_gc_mark_hook (sec, info, rel, h, sym);
}

/* Update the got entry reference counts for the section being removed.  */
static bfd_boolean csky_elf_gc_sweep_hook
    (bfd * abfd ATTRIBUTE_UNUSED,
     struct bfd_link_info * info ATTRIBUTE_UNUSED,
     asection * sec ATTRIBUTE_UNUSED,
     const Elf_Internal_Rela * relocs ATTRIBUTE_UNUSED)
{
  ASSERTLINE;
    return TRUE;
}
/* Look through the relocs for a section during the first phase.
   Since we don't do .gots or .plts, we just need to consider the
   virtual table relocs for gc.  */
static bfd_boolean csky_elf_check_relocs (bfd * abfd,
			struct bfd_link_info * info,
			asection * sec,
			const Elf_Internal_Rela * relocs)
{
  ASSERTLINE;
  Elf_Internal_Shdr * symtab_hdr;
  struct elf_link_hash_entry ** sym_hashes;
  const Elf_Internal_Rela * rel;
  const Elf_Internal_Rela * rel_end;
  struct csky_elf_link_hash_table *htab;
  asection *sreloc;

  if (info->relocatable)
    return TRUE;

  htab = csky_elf_hash_table (info);
  symtab_hdr = & elf_tdata (abfd)->symtab_hdr;
  sym_hashes = elf_sym_hashes (abfd);

  rel_end = relocs + sec->reloc_count;
  sreloc = NULL;
  for (rel = relocs; rel < rel_end; rel++)
    {
      struct elf_link_hash_entry * h;
      unsigned long r_symndx;
      Elf_Internal_Sym *isym;

      r_symndx = ELF32_R_SYM (rel->r_info);

      if (r_symndx < symtab_hdr->sh_info)
      {
        /* A local symbol.  */
        isym = bfd_sym_from_r_symndx (&htab->sym_cache,
                                      abfd, r_symndx);
        if (isym == NULL)
          return FALSE;

        h = NULL;
      }
      else
	{
          isym = NULL;
	  h = sym_hashes [r_symndx - symtab_hdr->sh_info];
	  while (h->root.type == bfd_link_hash_indirect
		 || h->root.type == bfd_link_hash_warning)
	    h = (struct elf_link_hash_entry *) h->root.u.i.link;
	}

      switch (ELF32_R_TYPE (rel->r_info))
        {
        case R_CKCORE_ADDR32:
          if (h != NULL && info->executable)
            {
              /* If this reloc is in a read-only section, we might
                 need a copy reloc.  We can't check reliably at this
                 stage whether the section is read-only, as input
                 sections have not yet been mapped to output sections.
                 Tentatively set the flag for now, and correct in
                 adjust_dynamic_symbol.  */
              h->non_got_ref = 1;

            }
          /* If we are creating a shared library, and this is a reloc
             against a global symbol, or a non PC relative reloc
             against a local symbol, then we need to copy the reloc
             into the shared library.  However, if we are linking with
             -Bsymbolic, we do not need to copy a reloc against a
             global symbol which is defined in an object we are
             including in the link (i.e., DEF_REGULAR is set).  At
             this point we have not seen all the input files, so it is
             possible that DEF_REGULAR is not set now but will be set
             later (it is never cleared).  In case of a weak definition,
             DEF_REGULAR may be cleared later by a strong definition in
             a shared library.  We account for that possibility below by
             storing information in the relocs_copied field of the hash
             table entry.  A similar situation occurs when creating
             shared libraries and symbol visibility changes render the
             symbol local.

             If on the other hand, we are creating an executable, we
             may need to keep relocations for symbols satisfied by a
             dynamic library if we manage to avoid copy relocs for the
             symbol.  */
          if ((info->shared
               && (sec->flags & SEC_ALLOC) != 0)
              || (!info->shared
                  && (sec->flags & SEC_ALLOC) != 0
                  && h != NULL
                  && (h->root.type == bfd_link_hash_defweak
                      || !h->def_regular)))
            { 
              struct csky_elf_dyn_relocs *p;
              struct csky_elf_dyn_relocs **head;

              /* We must copy these reloc types into the output file.
                 Create a reloc section in dynobj and make room for
                 this reloc.  */
              if (sreloc == NULL) 
                {
                  if (htab->elf.dynobj == NULL)
                    htab->elf.dynobj = abfd;

                  sreloc = _bfd_elf_make_dynamic_reloc_section
                    (sec, htab->elf.dynobj, 2, abfd, /*rela?*/ TRUE);

                  if (sreloc == NULL)
                    return FALSE;
                }

              /* If this is a global symbol, we count the number of
                 relocations we need for this symbol.  */
              if (h != NULL)
                {
                  head = &((struct csky_elf_link_hash_entry *) h)->dyn_relocs;
                }
              else
                {
                  /* Track dynamic relocs needed for local syms too.
                     We really need local syms available to do this
                     easily.  Oh well.  */
                  void **vpp;
                  asection *s;
                  Elf_Internal_Sym *isym;

                  isym = bfd_sym_from_r_symndx (&htab->sym_cache,
                                                abfd, r_symndx);
                  if (isym == NULL)
                    return FALSE;

                  s = bfd_section_from_elf_index (abfd, isym->st_shndx);
                  if (s == NULL)
                    s = sec;

                  vpp = &elf_section_data (s)->local_dynrel;
                  head = (struct csky_elf_dyn_relocs **)vpp;
                }

              p = *head;
              if (p == NULL || p->sec != sec)
                {
                  bfd_size_type amt = sizeof *p;
                  p = ((struct csky_elf_dyn_relocs *)
                       bfd_alloc (htab->elf.dynobj, amt));
                  if (p == NULL)
                    return FALSE;
                  p->next = *head;
                  *head = p;
                  p->sec = sec;
                  p->count = 0; 
                  p->pc_count = 0;
                }

              p->count += 1;
            }
          break;

        case R_CKCORE_GOT32:
        case R_CKCORE_PLT32:
            if (h != NULL)
              {
                //if (h->got.offset != (bfd_vma) -1)
                //  /* We have already allocated space in the .got.  */
                //  break;

                h->got.refcount += 1;
                //h->elf.srelgot.refcount += 1;
              }
            else
              {
                bfd_signed_vma *local_got_refcounts;

                /* This is a global offset table entry for a local symbol.  */
                local_got_refcounts = elf_local_got_refcounts (abfd);
                if (local_got_refcounts == NULL)
                  {
                    bfd_size_type size;

                    size = symtab_hdr->sh_info;
                    size *= (sizeof (bfd_signed_vma) + sizeof(char));
                    local_got_refcounts = ((bfd_signed_vma *)
                                           bfd_zalloc (abfd, size));
                    if (local_got_refcounts == NULL)
                      return FALSE;
                    elf_local_got_refcounts (abfd) = local_got_refcounts;
                  }
                local_got_refcounts[r_symndx] += 1;
              }
          /* Fall through */

        case R_CKCORE_GOTOFF:
        case R_CKCORE_GOTPC:
          if (htab->elf.sgot == NULL)
            {
              if (htab->elf.dynobj == NULL)
                htab->elf.dynobj = abfd;
              if (!_bfd_elf_create_got_section(htab->elf.dynobj, info))
                return FALSE;
            }
          break;

        /* This relocation describes the C++ object vtable hierarchy.
           Reconstruct it for later use during GC.  */
        case R_CKCORE_GNU_VTINHERIT:
          if (!bfd_elf_gc_record_vtinherit (abfd, sec, h, rel->r_offset))
            return FALSE;
          break;

        /* This relocation describes which C++ vtable entries are actually
           used.  Record for later use during GC.  */
        case R_CKCORE_GNU_VTENTRY:
          BFD_ASSERT (h != NULL);
          if (h != NULL
              && !bfd_elf_gc_record_vtentry (abfd, sec, h, rel->r_addend))
            return FALSE;
          break;
        }
    }

  return TRUE;
}

static const struct bfd_elf_special_section csky_elf_special_sections[]= {
  { STRING_COMMA_LEN (".ctors"), -2, SHT_PROGBITS, SHF_ALLOC + SHF_WRITE },
  { STRING_COMMA_LEN (".dtors"), -2, SHT_PROGBITS, SHF_ALLOC + SHF_WRITE },
  { NULL,                     0,  0, 0,            0 }
};

static bfd_boolean
csky_elf_set_private_flags (bfd * abfd, flagword flags)
{
  ASSERTLINE;
  BFD_ASSERT (! elf_flags_init (abfd)
	      || elf_elfheader (abfd)->e_flags == flags);

  elf_elfheader (abfd)->e_flags = flags;
  elf_flags_init (abfd) = TRUE;
  return TRUE;
}

static bfd_boolean
csky_elf_merge_private_bfd_data (bfd * ibfd, bfd * obfd)
{
  ASSERTLINE;
  flagword old_flags;
  flagword new_flags;

  /* Check if we have the same endianess.  */
  if (! _bfd_generic_verify_endian_match (ibfd, obfd))
    return FALSE;

  if (   bfd_get_flavour (ibfd) != bfd_target_elf_flavour
      || bfd_get_flavour (obfd) != bfd_target_elf_flavour)
    return TRUE;

  new_flags = elf_elfheader (ibfd)->e_flags;
  old_flags = elf_elfheader (obfd)->e_flags;

  if (! elf_flags_init (obfd))
    {
      	/* First call, no flags set.  */
      elf_flags_init (obfd) = TRUE;
      elf_elfheader (obfd)->e_flags = new_flags;
    }
  else if(new_flags == old_flags)
    {
      // do nothing
    }
  else if((((new_flags | old_flags) & (M_CK510 | M_CK610)) &&
           ((new_flags | old_flags) & (M_CK803 | M_CK810)))     // 8xx && 5xx
//          (((new_flags == 0) && (old_flags & (M_CK803 | M_CK810))) || 
//           ((old_flags == 0) && (new_flags & (M_CK803 | M_CK810))) )  
//            8xx && 0 (no eflag means ld -bbinary f.bin f.o)  
         )
    {
      const char *msg;

      msg = _("%B: machine flag conflict with target");

      (*_bfd_error_handler) (msg, ibfd);
      bfd_set_error (bfd_error_wrong_format);
      return FALSE;
      
    }
  else
    {
      old_flags |= new_flags;
      if((old_flags & M_CK510) && (old_flags & M_CK610))
      {
        old_flags &= ~M_CK510;
      }

      if((old_flags & M_CK803) && (old_flags & M_CK810))
      {
        old_flags &= ~M_CK803;
      }
      elf_elfheader (obfd)->e_flags = old_flags;
    }
  return TRUE;
}

static bfd_boolean
csky_elf_grok_prstatus (bfd *abfd, Elf_Internal_Note *note)
{
  int offset;
  size_t size;

  switch (note->descsz)
    {
      default:
        return FALSE;
      case 148 :            /* sizeof(struct elf_prstatus) on csky of CK610 */
      /* pr_cursig */
        elf_tdata (abfd)->core_signal
          = bfd_get_16 (abfd, note->descdata + 12);
         /* pr_pid */
        elf_tdata (abfd)->core_pid
          = bfd_get_32 (abfd, note->descdata + 24);
         /* pr_reg */
        offset = 72;
        size = 72;      /* 18 * 4 */

       break;
     case 220 :		/* sizeof(struct elf_prstatus) on csky of CK810 */
       /* pr_cursig */
        elf_tdata (abfd)->core_signal
          = bfd_get_16 (abfd, note->descdata + 12);
         /* pr_pid */
        elf_tdata (abfd)->core_pid
          = bfd_get_32 (abfd, note->descdata + 24);
         /* pr_reg */
        offset = 72;
        size = 34 * 4;      /* 34 * 4 */

       break;

    }
   /* Make a ".reg/999" section.  */
  return _bfd_elfcore_make_pseudosection (abfd, ".reg",
                                          size, note->descpos + offset);
}

static bfd_boolean
csky_elf_grok_psinfo (bfd *abfd, Elf_Internal_Note *note)
{
  switch (note->descsz)
    {
      default:
        return FALSE;

      case 124:         /* sizeof(struct elf_prpsinfo) on linux csky */
        elf_tdata (abfd)->core_program
         = _bfd_elfcore_strndup (abfd, note->descdata + 28, 16);
        elf_tdata (abfd)->core_command
         = _bfd_elfcore_strndup (abfd, note->descdata + 44, 80);
    }
 /* Note that for some reason, a spurious space is tacked
     onto the end of the args in some (at least one anyway)
     implementations, so strip it off if it exists.  */

  {
    char *command = elf_tdata (abfd)->core_command;
    int n = strlen (command);

    if (0 < n && command[n - 1] == ' ')
      command[n - 1] = '\0';
  }

  return TRUE;
}

#define TARGET_BIG_SYM		bfd_elf32_csky_big_vec
#define TARGET_BIG_NAME		"elf32-csky-big"
#define TARGET_LITTLE_SYM       bfd_elf32_csky_little_vec
#define TARGET_LITTLE_NAME      "elf32-csky-little"

#define ELF_ARCH		bfd_arch_csky
#define ELF_MACHINE_CODE	EM_CSKY
#define ELF_MAXPAGESIZE		0x1000
#define elf_info_to_howto	csky_elf_info_to_howto
#define elf_info_to_howto_rel	NULL

#define bfd_elf32_bfd_merge_private_bfd_data	csky_elf_merge_private_bfd_data
#define bfd_elf32_bfd_set_private_flags		csky_elf_set_private_flags
#define bfd_elf32_bfd_reloc_type_lookup		csky_elf_reloc_type_lookup
#define bfd_elf32_bfd_reloc_name_lookup		csky_elf_reloc_name_lookup
#define elf_backend_relocate_section		csky_elf_relocate_section
#define elf_backend_gc_mark_hook		csky_elf_gc_mark_hook
#define elf_backend_gc_sweep_hook		csky_elf_gc_sweep_hook
#define elf_backend_check_relocs                csky_elf_check_relocs
#define elf_backend_special_sections		csky_elf_special_sections

#define elf_backend_can_gc_sections		1
#define elf_backend_rela_normal			1
#define elf_backend_can_refcount                1
#define elf_backend_plt_readonly                1
#define elf_backend_want_got_sym                1
#define elf_backend_got_header_size             12


// added by liub start
#define elf_backend_reloc_type_class            csky_elf_reloc_type_class
#define bfd_elf32_bfd_link_hash_table_create    csky_elf_link_hash_table_create

#define elf_backend_create_dynamic_sections    csky_elf_create_dynamic_sections
#define elf_backend_adjust_dynamic_symbol      csky_elf_adjust_dynamic_symbol
#define elf_backend_size_dynamic_sections      csky_elf_size_dynamic_sections
#define elf_backend_finish_dynamic_symbol      csky_elf_finish_dynamic_symbol
#define elf_backend_finish_dynamic_sections    csky_elf_finish_dynamic_sections

// csky coredump support
#define elf_backend_grok_prstatus              csky_elf_grok_prstatus
#define elf_backend_grok_psinfo                csky_elf_grok_psinfo

#include "elf32-target.h"

/* if more backend targets are needed, we could write:

#undef TARGET_BIG_SYM
#define TARGET_BIG_SYM xxxx
#undef TARGET_BIG_NAME
...
...
#include "elf32-target.h"

*/
