/* BFD library support routines for Motorola's MCore architecture
   Copyright 1993, 1999, 2000, 2002, 2005, 2007 Free Software Foundation, Inc.

   This file is part of BFD, the Binary File Descriptor library.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */

#include "sysdep.h"
#include "bfd.h"
#include "libbfd.h"

#define N(NUMBER, PRINT, ISDEFAULT, NEXT)  \
{                                                                      \
  32,               /* 32 bits in a word */                            \
  32,               /* 32 bits in an address */                        \
  8,                /* 8 bits in a byte */                             \
  bfd_arch_csky,    /* Architecture */                                 \
  NUMBER,           /* Machine number */                               \
  "csky",           /* Architecture name */                            \
  PRINT,            /* Printable name */                               \
  3,                /* Section align power */                          \
  ISDEFAULT,          /* Is this the default architecture ? */           \
  bfd_default_compatible,   /* Architecture comparison function */     \
  bfd_default_scan, /* String to architecture conversion */            \
  NEXT              /* Next in list */                                 \
}   

static const bfd_arch_info_type arch_info_struct[] =
  { 
/* abi v1 */
    N(bfd_mach_ck510e,  "csky:ck510e",   FALSE, &arch_info_struct[1]),
    N(bfd_mach_ck520,   "csky:ck520",    FALSE, &arch_info_struct[2]),
    N(bfd_mach_ck610,   "csky:ck610",    FALSE, &arch_info_struct[3]),
    N(bfd_mach_ck610e,  "csky:ck610e",   FALSE, &arch_info_struct[4]),
    N(bfd_mach_ck610f,  "csky:ck610f",   FALSE, &arch_info_struct[5]),
    N(bfd_mach_ck610ef, "csky:ck610ef",  FALSE, &arch_info_struct[6]),
    N(bfd_mach_ck620,   "csky:ck620",    FALSE, &arch_info_struct[7]),
    N(bfd_mach_ck802p,  "csky:ck802p",   FALSE, &arch_info_struct[8]),   
    N(bfd_mach_ck803p,  "csky:ck803p",   FALSE, &arch_info_struct[9]),
    N(bfd_mach_ck810p,  "csky:ck810p",   FALSE, &arch_info_struct[10]),
    N(bfd_mach_ck803a,  "csky:ck803a",   FALSE, &arch_info_struct[11]),

/* abi v2 */
    N(bfd_mach_ck802,   "csky:ck802",    FALSE, &arch_info_struct[12]),
    N(bfd_mach_ck802e,  "csky:ck802e",   FALSE, &arch_info_struct[13]),
    N(bfd_mach_ck802j,  "csky:ck802j",   FALSE, &arch_info_struct[14]),
    N(bfd_mach_ck803,   "csky:ck803",    FALSE, &arch_info_struct[15]),
    N(bfd_mach_ck803e,  "csky:ck803e",   FALSE, &arch_info_struct[16]),
    N(bfd_mach_ck803j,  "csky:ck803j",   FALSE, &arch_info_struct[17]),
    N(bfd_mach_ck810,   "csky:ck810",    FALSE, &arch_info_struct[18]),
    N(bfd_mach_ck810e,  "csky:ck810e",   FALSE, &arch_info_struct[19]),
    N(bfd_mach_ck810f,  "csky:ck810f",   FALSE, &arch_info_struct[20]),
    N(bfd_mach_ck810ef, "csky:ck810ef",  FALSE, &arch_info_struct[21]),
    N(bfd_mach_ck810j,  "csky:ck810j",   FALSE, &arch_info_struct[22]),
    N(bfd_mach_ck807,   "csky:ck807",    FALSE, &arch_info_struct[23]),
    N(bfd_mach_ck807f,  "csky:ck807f",   FALSE, &arch_info_struct[24]),
    N(bfd_mach_ck807e,  "csky:ck807e",   FALSE, &arch_info_struct[25]),
    N(bfd_mach_ck807ef, "csky:ck807ef",  FALSE, &arch_info_struct[26]),
    N(bfd_mach_ck807j,  "csky:ck807j",   FALSE, &arch_info_struct[27]),
    N(bfd_mach_ck803s,  "csky:ck803s",   FALSE, &arch_info_struct[28]),
    N(bfd_mach_ck803se, "csky:ck803se",  FALSE, &arch_info_struct[29]),
    N(bfd_mach_ck803sj, "csky:ck803sj",  FALSE, &arch_info_struct[30]),
    N(bfd_mach_ck803sf, "csky:ck803sf",  FALSE, &arch_info_struct[31]),
    N(bfd_mach_ck803sef,"csky:ck803sef", FALSE, &arch_info_struct[32]),
    N(bfd_mach_ck801,   "csky:ck801",    FALSE, NULL)
  };

const bfd_arch_info_type bfd_csky_arch =
  N(bfd_mach_ck510,   "csky",  TRUE, &arch_info_struct[0]);


